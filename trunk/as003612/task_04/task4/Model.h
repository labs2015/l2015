///@file Model.h
#pragma once
/**
@mainpage ������
@image html graphics.png
Linear: ������ ���-�� �������� �������� �.�. ��� ���������� ������� ���������� ����������� ����� � ������.
NonLinear: ������ ���-�� �������� ���������� �.�. ���� �������� ������� ���������� ����������� ����� � ������.
*/

/// \brief ������� ����������� �����
class Model
{
public:
	Model();
	virtual ~Model();
	//methods
	/// \brief ������ ����������� ����� ��� ���������� ������� ���������.
	virtual void func(double, double) = 0;
};

