/// \file task4.cpp
/// \warning ������ ��������� ����������� ������������� � ������� �����.
/// \author ��������� �.�.

#include "stdafx.h"
#include "Model.h"
#include "Linear.h"
#include "NonLinear.h"
#include <iostream>


/// \brief _tmain �������� ������ ����� � ���������
int _tmain(int argc, _TCHAR* argv[])
{
	Model* obj = new Linear();
	std::cout << "Linear model" << std::endl;
	obj->func(12, 23);
	obj = new NonLinear();
	std::cout << "NonLinear model" << std::endl;
	obj->func(12, 23);
	return 0;
}

