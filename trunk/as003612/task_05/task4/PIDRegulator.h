#pragma once
/// �����, ������������ �������� �����������
class PIDRegulator
{
private:
	double q0, q1, q2;
	double T0, Td, T;
	double K;
	double eFirst, eSec, eThird;
	double u;

public:
	PIDRegulator();
	PIDRegulator(double, double, double, double);
	/// ����������� ut
	///@param y -  ����������� ������������
	///@param w - �������� ���������������� �������
	///@return - ut
	double Deviation(double, double);
	~PIDRegulator();
};
