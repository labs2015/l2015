/// \file task4.cpp
/// \warning ������ ��������� ����������� ������������� � ������� �����.
/// \author ��������� �.�.

#include "stdafx.h"
#include "Model.h"
#include "Linear.h"
#include "NonLinear.h"
#include "PIDRegulator.h"
#include <iostream>

using namespace std;
/// \brief _tmain �������� ������ ����� � ���������
int _tmain(int argc, _TCHAR* argv[])
{
	const double w = 75.0;

	Linear *linMod = new Linear();
	NonLinear *nonlinMod = new NonLinear(0);
	PIDRegulator *regul = new PIDRegulator(0.01, 0.5, 0.21, 0.5);

	double y = linMod->func(0, 0);
	double u = regul->Deviation(y, w);

	for (int i = 0; i < 200; i++)
	{
		y = linMod->func(y, u);
		u = regul->Deviation(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}
	cout << "NonLinear Model: " << endl;

	y = nonlinMod->func(0, 0);
	u = regul->Deviation(y, w);

	for (int i = 0; i < 200; i++)
	{
		y = nonlinMod->func(y, u);
		u = regul->Deviation(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}

	system("PAUSE");
	return 0;
}

