///@file NonLinear.cpp

#include "stdafx.h"
#include "NonLinear.h"
#include <iostream>
#include <math.h>

NonLinear::NonLinear()
{
}

NonLinear::NonLinear(double y0)
{
	this->ytPrev = y0;
}


NonLinear::~NonLinear()
{
}

///@param "in_" - �������� �����������
///@param "out_" - �����
double NonLinear::func(double in_, double out_)
{
	double resin_ = 0;
	resin_ = 0.9*in_ - 0.001*ytPrev*ytPrev + out_ + sin(out_);
	ytPrev = in_;
	return resin_;
}