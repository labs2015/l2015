///@file NonLinear.h
#pragma once
#include "Model.h"
/// \brief ����� ��������� "Model", ����������� ���������� ������ ��������� �������.
class NonLinear :
	public Model
{
private:
	double ytPrev;
public:
	NonLinear();
	NonLinear(double);
	~NonLinear();
	/// \brief ���������� Model::func

	double func(double, double);
};

