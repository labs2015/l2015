///@file Model.h
#pragma once
/**
@mainpage ������
@image html graphics.png
�� �������� ����� ��� �� ���������� ������.

���������� ������ ������������ ������� ��� ��������.
*/

/// \brief ������� ����������� �����
class Model
{
public:
	Model();
	virtual ~Model();
	//methods
	/// \brief ������ ����������� ����� ��� ���������� ������� ���������.
	virtual double func(double, double) = 0;
};

