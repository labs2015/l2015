
#include "LinearModel.h"
#include<iostream>

LinearModel::LinearModel()
{
}

LinearModel::LinearModel(double qn, double wn)
{
	this->wn = wn;
	this->qn = qn;
}

void LinearModel::PrintModel(int n)
{
	int i = 0;
	while (i < n)
	{
		qn = 0.988*qn + 0.232*wn;
		std::cout << qn << std::endl;
		i++;
	}
};

LinearModel::~LinearModel()
{
}
