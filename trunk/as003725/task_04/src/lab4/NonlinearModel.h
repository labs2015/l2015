#pragma once
///@image html NonlinearGraphic.png
#include "GeneralModel.h"
///@brief �������� �����, ����������� ���������� ������
/**
������� �������� ���������� ��� ��� ������������ ��������, � ���������� ������������ ����� � ������.

������ ���������� ���������� ����������� ����� �� ������ �������.
*/
class NonlinearModel :
	public GeneralModel
{
private:
	double qn;
	double wn;
public:
	NonlinearModel();
	///@brief ������� �������� �������� ������������� � ���������� �������� �������� �����������
	///@param q0 - ��������� ������� �������� �����������
	///@param wn - ������� �������� �������������
	NonlinearModel(double q0, double wn);
	///@brief ����� �� ������� �������� �������� ����������� �� ������ ��������(���������� ������)
	///@param n - ���������� ��������
	void PrintModel(int n);
	~NonlinearModel();
};



