#pragma once
///@image html LinearGraphic.png
/**
������� �������� ��������, �.�. �����������  �������� ������� ���������� ����������� ����� � ������.

������� ���������� �������� ����������� ����� �� ������ �������.
*/
#include "GeneralModel.h"
///@brief �������� �����, ������� ��������� �������� ������
class LinearModel :
	public GeneralModel
{
private:
	double qn;
	double wn;
public:
	LinearModel();
	///@brief �������� ��������� ������� �������� ����������� � ������� �������� �������������
	///@param qn - ��������� �������� ����������� ����������� �� ����
	///@param wn - �������� ������������� ����������� �� ����
	LinearModel(double qn, double wn);
	///@brief ����� �� ������� �������� �� ���� �������� ����������� �� ������ ��������(�������� ������.)
	///@param n - ���������� ��������
	void PrintModel(int n);
	~LinearModel();
};
