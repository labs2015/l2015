///@file
///@author ����� ����
///@date 26.05.2015
#include <iostream>

using namespace std;

class Object
{
public:
	///@brief ���������� ���������� � �������� ����������� �������
	virtual double getTemperature(double y, double u) = 0;
};

class Object1 : public Object
{
public:


	double getTemperature(double y, double u)
	{
		return 0.988*y + 0.232*u;
	}
};


class Object2 : public Object
{
protected:
	double yL = 20;

public:

	///@brief ���������� �������� �������� ����������� �������
	double getY1()
	{
		return yL;
	}

	///@brief ������������� �������� �������� ����������� �������
	void setY1(double y)
	{
		this->yL = y;
	}

	double getTemperature(double y, double u)
	{
		double result = 0.9*y - 0.001*yL*yL + u + sin(u);
		yL = y;
		return result;
	}
};





class PID
{
private:
	double T0, Td, T, k, q0, q1, q2, e1 = 0, e2 = 0, e3 = 0, u = 0;
public:
	///@brief ������������� �������� ��� T0
	void SetT0(double t)
	{
		T0 = t;
	}
	double getT0()
	{
		return T0;
	}
	///@brief ������������� �������� ��� Td
	void SetTd(double t)
	{
		Td = t;
	}
	double getTd()
	{
		return Td;
	}
	///@brief ������������� �������� ��� T
	void SetT(double t)
	{
		T = t;
	}
	double getT()
	{
		return T;
	}
	///@brief ������������� �������� ��� �
	void SetK(double K)
	{
		k = K;
	}
	double getK()
	{
		return k;
	}
	///@brief ������������ �������� q
	void ColculateQ()
	{
		q0 = k * (1 + (Td / T0));
		q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = k * (Td / T0);
	}

	double Colculation(double y, double w)
	{
		e3 = e2;
		e2 = e1;
		e1 = w - y;
		u += (q0 * e1 + q1 * e2 + q2 * e3);
		return u;
	}

};

///@brief ������ ������������� ����������� �������
int main()
{
	Object1 o1;
	Object2 o2;
	PID pid;
	pid.SetK(0.5);
	pid.SetT(0.5);
	pid.SetT0(0.2);
	pid.SetTd(0.01);
	pid.ColculateQ();
	o2.setY1(0);

	double w = 50, y = o1.getTemperature(0, 0), u = pid.Colculation(y, w);

	cout.width(8);
	cout << "y" << "  |  " << "u" << endl;
	cout << "-------------------" << endl;

	for (int i = 0; i < 50; i++)
	{
		y = o1.getTemperature(y, u);
		u = pid.Colculation(y, w);
		cout.width(8);
		cout << y << "  |  " << u << endl;
	}

	cout << endl << endl;


	y = o2.getTemperature(0, 0);
	u = pid.Colculation(y, w);

	cout.width(8);
	cout << "y" << "  |  " << "u" << endl;
	cout << "-------------------" << endl;

	for (int i = 0; i < 50; i++)
	{
		y = o2.getTemperature(y, u);
		u = pid.Colculation(y, w);
		cout.width(8);
		cout << y << "  |  " << u << endl;
	}

	system("Pause");
}