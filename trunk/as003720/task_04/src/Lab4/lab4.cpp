﻿/**
@file
@author Сковородко П.В.
@date 13.05.15
*/
#include <iostream>
using namespace std;

class Model
{
protected:
	double y0 = 0;
public:
	///@brief Метод, возвращающий начальную выходную температуру
	double get_y0()
	{
		return y0;
	}
	///@brief Метод, устанавливающий начальную выходную температуру
	void set_y0(double y0)
	{
		this->y0 = y0;
	}
	///@brief Метод, выводящий выходную температуру
	virtual void showTemperature(double (*u)(double)) = 0;
};

	class Linear : public Model
	{
	public:
		void showTemperature(double (*ut)(double))
		{
			cout << "y\t\t" << "u" << endl;
			cout << "____________________\n";
			cout << endl << y0 << "\t\t" << ut(0) << endl;
			double y = y0;
			for (int i = 1; i < 20; i++)
			{
				cout << (y = 0.988 * y + 0.232 * ut(i - 1)) << "\t\t" << ut(i - 1) << endl;
			}
		}
	};

	class Non_Linear : public Model
	{
	protected:
		double y1 = 20;

	public:
		///@brief Метод, возвращающий новую выходную температуру
		double get_y1()
		{
			return y1;
		}
		///@brief Метод, устанавливающий новую выходную температуру
		void set_y1(double y1)
		{
			this->y1 = y1;
		}

		void showTemperature(double (*ut)(double))
		{
			cout << "y\t\t" << "u" << endl;
			cout << "____________________\n";
			cout << endl << y0 << "\t\t" << ut(0) << endl;
			cout << y1 << "\t\t" << ut(1) << endl;
			double yPrev = y0, yNext = y1;
			for (int i = 2; i < 20; i++)
			{
				double y = yNext;
				cout << (yNext = 0.9 * yNext - 0.001 * yPrev * yPrev + ut(i - 1) + sin(ut(i - 2))) << "\t\t" << ut(i - 1) << endl;
				yPrev = y;
			}
			cout << endl;
		}
	};

	///@brief Метод, возвращающий значение тепла, поступающего в систему
	double u(double t)
	{
		return 45.1;
	}

	///@brief Точка входа в программу
	void main()
	{
		Linear o1;
		Non_Linear o2;
		o1.showTemperature(&u);
		cout << endl << endl;
		o2.showTemperature(&u);
		system("PAUSE");
	}