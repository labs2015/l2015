﻿/**
@file
@author Сковородко Павел Владимирович
@date 26.03.15
*/
#include <iostream>

using namespace std;
/**
@brief Консольное приложение, выводящее на экран "Hello World!"
*/
int main() {
	cout << "Hello World!" << endl;

	return 0;
}
