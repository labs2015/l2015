/**
@file
@author ���������� �.�.
@date 27.05.2015
*/
#include <iostream>
using namespace std;

class Model
{
public:
	///@brief �����, ��������� �������� �����������
	virtual double showTemperature(double y, double u) = 0;
};

class Linear : public Model
{
public:
	double showTemperature(double y, double u)
	{
		return 0.988 * y + 0.232 * u;
	}
};

class Non_Linear : public Model
{
protected:
	double y1 = 20;
public:
	///@brief �����, ������������ ����� �������� �����������
	double get_y1()
	{
		return y1;
	}
	///@brief �����, ��������������� ����� �������� �����������
	void set_y1(double y)
	{
		this->y1 = y;
	}

	double showTemperature(double y, double u)
	{
		y1 = y;
		double res = 0.9 * y - 0.001 * y1 * y1 + u + sin(u);
		return res;
	}
};

class REGULATOR
{
private:
	double T0, Td, T, K, q0, q1, q2, e1 = 0, e2 = 0, e3 = 0, u = 0;
public:
	///@brief �����, ��������������� �������� T0
	void set_T0(double t0)
	{
		T0 = t0;
	}
	double get_T0()
	{
		return T0;
	}
	///@brief �����, ��������������� �������� Td
	void set_Td(double td)
	{
		Td = td;
	}
	double get_Td()
	{
		return Td;
	}
	///@brief �����, ��������������� �������� T
	void set_T(double t)
	{
		T = t;
	}
	double get_T()
	{
		return T;
	}
	///@brief �����, ��������������� �������� K
	void set_K(double k)
	{
		K = k;
	}
	double get_K()
	{
		return K;
	}
	///@brief �����, ������������� �������� q
	void calc_q()
	{
		q0 = K * (1 + (Td / T0));
		q1 = -K *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = K * (Td / T0);
	}
	double calc(double y, double w)
	{
		e3 = e2;
		e2 = e1;
		e1 = w - y;
		u += (q0 * e1 + q1 * e2 + q2 * e3);
		return u;
	}
};

///@brief ����� ����� � ���������
void main()
{
	Linear ob1;
	Non_Linear ob2;
	REGULATOR reg;
	reg.set_K(0.5);
	reg.set_T(0.5);
	reg.set_T0(0.2);
	reg.set_Td(0.01);
	reg.calc_q();
	ob2.set_y1(0);
	double w = 50, y = ob1.showTemperature(0, 0), u = reg.calc(y, w);
	cout << "y\t\t" << "u" << endl;
	cout << "____________________\n";
	for (int i = 0; i < 50; i++)
	{
		y = ob1.showTemperature(y, u);
		u = reg.calc(y, w);
		cout << y << "\t\t" << u << endl;
	}
	y = ob2.showTemperature(0, 0);
	u = reg.calc(y, w);
	cout << "\n\ny\t\t" << "u" << endl;
	cout << "____________________\n";
	for (int i = 0; i < 50; i++)
	{
		y = ob2.showTemperature(y, u);
		u = reg.calc(y, w);
		cout << y << "\t\t" << u << endl;
	}
	system("PAUSE");
}