/// \file main.cpp
/*!
\author ������� ���� ��������� ��-36
\date ��� 2015
*/
/*!
\mainpage ������ � �������������� ���-����������
�� ����������� ������ ��������� ���� ��������� ��� �������, ������������ ������ PID-����������:
\image html LinPid.png "���.1 �������� ������ ���������"
\image html NonLinPid.png "���.2 ���������� ������ ���������" 
����� ��������, ��� ���������� ������ �������� ������� ��� ��������.
*/
#include<iostream>
#include"linear_model.h"
#include"nonlinear_model.h"
#include"PID_Regulator.h"
#include<iomanip>

#include<fstream>
using namespace std;
/// \brief ������� ����� � ���������
/// \details �������� ������������� ���-����������. ������� �� ������� �������� �����������.


int main()
{

	double W;
	cout << "Enter W:"; cin >> W;
	double Td=0.01, T=0.5, T0=0.21, K=0.5;
	linear_model lin;
	nonlinear_model nonlin(0,0);
	PID_Regulator reg(Td, T, T0, K);
	
	double Yt_1=0;
	double Yt = 0, Ut = 0;
	 Yt = lin.temperature(Yt_1,0);
	 Ut = reg.calculate(W,Yt);
	Yt_1 = Yt;
	cout << "Linear model:" << endl;
	cout << setw(9)<<"Y(" << 1 << ") = " << Yt << ";" << setw(9) << " U(" << 1 << ") = " << Ut << endl;
	for(int i = 0; i < 200; i++)
	{
		Yt = lin.temperature(Yt_1,Ut);
		Ut = reg.calculate(W,Yt);
		Yt_1 = Yt;
		cout <<setw(9)<< "Y(" << i + 2 << ") = " << Yt << "; " << setw(9) << "U(" << i + 2 << ") = " << Ut << endl;

	}
	
	PID_Regulator reg2(Td, T, T0, K);
	Yt_1=0, Yt = 0, Ut = 0;
	Yt = nonlin.temperature(0, 0);

	Ut = reg2.calculate(W, Yt);
	Yt_1 = Yt;
	cout << "nonlinear model:" << endl;
	cout <<setw(9)<< "Y(" << 1 << ") = " << Yt << ";"<<setw(9) <<" U(" << 1 << ") = " << Ut << endl;
	for (int i = 0; i < 200; i++)
	{
		Yt = nonlin.temperature(Yt_1, Ut);
		Ut = reg2.calculate(W, Yt);
		Yt_1 = Yt;
		cout <<setw(9)<< "Y(" << i + 2 << ") = " << Yt << ";"<<setw(9)<<" U(" << i + 2 << ") = " << Ut << endl;

	}
	
	system("pause");
	return 0;	
}