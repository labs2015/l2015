/// \file PID_Regulator.cpp
#include "PID_Regulator.h"
#include<iostream>
using namespace std;

PID_Regulator::PID_Regulator(double _Td, double _T, double _T0, double _K) :Td(_Td), T(_T), T0(_T0), K(_K)
{
	q0 = K * (1 + (Td / T0));
	q1 = -K *(1 + 2 * (Td / T0) - (T0 / T));
	q2 = K * (Td / T0);

	cout << "q0=" << q0 << endl;
	cout << "q1=" << q1 << endl;
	cout << "q2=" << q2 << endl;
	Ut = 0;

	et = 0;
	et_1 = 0;
	et_2 = 0;
}
/// ������� ut
///@param "Yt" - �������� ����������
///@param "Wt" - �������� ���������������� �������
double PID_Regulator::calculate(double Wt, double Yt)
{
	et_2 = et_1;
	et_1 = et;
	et = Wt - Yt;
	Ut += (q0*et + q1*et_1 + q2*et_2);
	return Ut;
}

PID_Regulator::~PID_Regulator()
{
}
