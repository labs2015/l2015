#pragma once
/// \brief �����, ������������ ������ PID-����������.

class PID_Regulator
{
private:
	double T;
	double Td;
	double T0;
	double K;
	double q0;
	double q1;
	double q2;
	
	
	double Ut;

	double et;
	double et_1;
	double et_2;
public:
	PID_Regulator(double,double,double,double);
	double calculate(double,double);
	~PID_Regulator();
};

