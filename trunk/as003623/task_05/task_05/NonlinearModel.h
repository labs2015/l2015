#pragma once
#include "General.h"
class NonlinearModel :
	public General
{
private:
	double ytPrev;
public:
	NonlinearModel();
	NonlinearModel(double y0);
	double Model(double yt, double ut);
	~NonlinearModel();
};



