///@file NLinearModel.cpp
#include "NLinearModel.h"
#include <iomanip>
using namespace std;
void NLinearModel::Modeling(int time, int samp, double ly, double u)
{
	int count = time / samp;
	ny = ly;
	for (int i = 0; i < count; i++)
	{
		y = 0.9*ly- 0.001*ny*ny + u + sin(u);
		cout  << y << endl;
		ny = ly;
		ly = y;
	}
}
