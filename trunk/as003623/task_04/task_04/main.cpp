///@file main.cpp
/// \author Ը����� �.�.
#include <iostream>
#include <iomanip>
#include <string>
#include "LinearModel.h"
#include "NLinearModel.h"
using namespace std;
///\mainpage My Project Documentation
///\image html LinModel.png
///\image html NONLinModel.png
///������ �������� ������� ����������� ������, �.�. ������ ����� ����� �������� �� ���������� �� �������� "���������� ����������� ����������� � ������������� 0,988 , � ����� ����� ��������� � ������������� 0.232", �.�. ������������ ���.
///������ ���������� ������� ���������� �� ������, �.�.������ ����� ����� ���������� �� ������ � �������������� ���������� ��������.




int tim, samp;
double y, u;
void linearModel()
{
	LinearModel lModel;
	lModel.Modeling(tim, samp, y, u);
}
void nlinearModel()
{
	NLinearModel nModel;
	nModel.Modeling(tim, samp, y, u);
}
/// \brief main-����� ����� � ���������.
int main()
{

	{
		cout << "Discrete time integer:" << endl;
		cin >> tim;
		cout << "The sampling:"<<endl;
		cin >> samp;
		cout << "Output temperature:" << endl;
		cin >>setw(25)>> y;
		cout << "Warm:" <<endl;
		cin >> u;
		cout << "Linear model:"<<endl;
		linearModel();
		cout  << "NONLinear model:" <<endl;
		nlinearModel();
	}
	system("pause");
	return 0;
}

