#pragma once
/// \brief ����� "obj" �������� ����������� ������� �������. �� ���� ����������� ������ "lineal" � "nonlineal"
class obj
{
protected:
	int t, T;
	double yt, ut;
public:
	virtual void Model() = 0;
	obj(int _t, int _T, double _yt, double _ut) :t(_t), T(_T), yt(_yt), ut(_ut)
	{
	}

	virtual ~obj()
	{
	}
};

