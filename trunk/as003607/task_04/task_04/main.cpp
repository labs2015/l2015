/**
@mainpage ������
@image html lin.png
Linear: ������ ������� �������� �������� �.�. ��� ��������� ������� ���������� ����������� ����� � ������.
@image html nelin.png
NonLinear: ������ ������� �������� ���������� �.�. ���� ��� ��������� ������� ���������� ����������� ����� ������ � �������.
*/
/// \file main.cpp
/// \brief ���������, ������������ ������ ����������, ��������� �������� � ���������� ������.
/// \author ���� �.�.
/// \date 10.05.2015
#include <iostream>
#include <string>
#include "lineal.h"
#include "nonlineal.h"
using namespace std;
/// \brief main-����� ����� � ���������.
int main()
{
	int time, qv, yt, ut;
	setlocale(LC_ALL, "Russian");
	cout << "������� ���������� ������������� �����:" << endl;
	cin >> time;
	cout << "������� �������� �����������:" << endl;
	cin >> qv;
	cout << "������� �������� �����������:" << endl;
	cin >> yt;
	cout << "������� �����:" << endl;
	cin >> ut;
	cout << "�������� ������:" << endl;
	lineal a1(time, qv, yt, ut);
	a1.Model();
	cout << "\n���������� ������:" << endl;
	nonlineal a2(time, qv, yt, ut);
	a2.Model();
	system("pause");
	return 0;
}