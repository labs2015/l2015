#pragma once
///�����, ����������� PID-���������
class Pid
{
private:
	double q0, q1, q2;
	double T, T0, Tdif;
	double e, e1, e2;
	double u, K;
public:
	Pid(double _T, double _T0, double _Tdif, double _K);

	///��������� u(t)
	///@param _w - �������� ���������������� �������
	///@param _y - �������� ����������
	///@return u(t) -  ����������� �����������

	double raschet(double _w, double _y);
	~Pid();
};

