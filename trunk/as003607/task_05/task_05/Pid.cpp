#include "Pid.h"

Pid::Pid(double _T, double _T0, double _Tdif, double _K) :T(_T), T0(_T0), Tdif(_Tdif), K(_K)
{
	u = 0;
	e = e1 = e2 = 0;
	q0 = K * (1 + (Tdif / T0));
	q1 = -K *(1 + 2 * (Tdif / T0) - (T0 / T));
	q2 = K * (Tdif / T0);
}

double Pid::raschet(double _w, double _y)
{
	e2 = e1;
	e1 = e;
	e = _w - _y;
	u += (q0 * e + q1 * e1 + q2 * e2);
	return u;
}


Pid::~Pid()
{
}
