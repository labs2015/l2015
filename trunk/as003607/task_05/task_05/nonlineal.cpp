#include "nonlineal.h"

nonlineal::nonlineal(double _yt, double _ut)
{
	yt = _yt;
	ut = _ut;
}

double nonlineal::Model(double _y, double _u)
{
	double y = 0.9*_y - 0.001*yt*yt + _u + sin(ut);
	yt = _y;
	ut = _u;
	return y;
}
