/*!
@author ���� ����� ������� ��-36
@date 25.05.2015
*/
/*!
@mainpage ������ � �������������� PID-����������
@image html lin.png
@image html nelin.png
������� ����������, ��� ���������� ������ �������� ������� ��� ��������
*/
#include <iostream>
#include <string>
#include <fstream>
#include "lineal.h"
#include "nonlineal.h"
#include "Pid.h"
using namespace std;
/// \brief main-����� ����� � ���������.
int main()
{
	setlocale(LC_ALL, "Russian");
		double w;
		Pid contr(0.5, 0.21, 0.01, 0.5);
		cout << "������� w:" << endl;
		cin >> w;
		cout << "\n�������� ������:" << endl;
		lineal lM;
		double y1 = lM.Model(0, 0);
		double u1 = contr.raschet(w, y1);
		for (int i = 0; i < 130; i++)
		{

			cout << "y= " << y1 << "\t" << "u= " << u1 << endl;
			y1 = lM.Model(y1, u1);
			u1 = contr.raschet(w, y1);
		}
		system("pause");
		cout << "\n���������� ������:" << endl;
		nonlineal nM(0, 0);
		double y2 = nM.Model(0, 0);
		double u2 = contr.raschet(w, y2);
		for (int i = 0; i < 130; i++)
		{
			cout << "y= " << y2 << "\t" << "u= " << u2 << endl;
			y2 = nM.Model(y2, u2);
			u2 = contr.raschet(w, y2);
		}
		system("pause");
	return 0;
}

