/// \file temperature.cpp
/// \brief class temperature file
#include "temperature.h"
#include <math.h>
#include <iostream>
using namespace std;

temperature::temperature()
{
	{
		first = 0;
		last = 100;
	}

	for (int i = 0; i < 100; i++)
	{
		ylinear[i] = 0;
		ynonlinear[i] = 0;
	}

	///������������� ���� �����
}
temperature::temperature(int First, int Last)
{
	{
		first = First;
		last = Last;
	}
	for (int i = first; i < last; i++)
	{
		ylinear[i] = 0;
		ynonlinear[i] = 0;
	}
	///������������� ���� �����
}

void temperature::lineart(int start, int end)
{
	/// u(t) = const = 1
	///
	/// �������������� �������� y(t) = 0
	ylinear[start] = 0.988 * 0 + 0.232 * 1;
	for (int i = start + 1; i < end; i++)
		ylinear[i] = 0.988 * ylinear[i - 1] + 0.232 * 1;
}

void temperature::nonlineart(int start, int end)
{
	/// u(t) = const = 1
	///
	/// �������������� �������� y(t) = 0
	///
	/// �������������� �������� y(t - 1) = 0
	ynonlinear[start] = 0.9 * 0 - 0.001 * 0 + 1 + sin((double)1);
	ynonlinear[start + 1] = 0.9 * ynonlinear[start] + 1 + sin((double)1);
	for (int i = start + 2; i < end; i++)
	{
		ynonlinear[i] = 0.9 * ynonlinear[i - 1] - 0.001 * pow((double)(ynonlinear[i - 2]), 2) + 1 + sin((double)1);
	}
}

void temperature::print()
{
	cout << "Linear:" << endl;
	for (int i = first; i < last; i++)
		cout << ylinear[i] << endl;
	cout << "NONLinear:" << endl;
	for (int i = first; i < last; i++)
		cout << ynonlinear[i] << endl;

	///@image html linear.png
	///@image html nonlinear.png
}