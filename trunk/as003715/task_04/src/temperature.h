#pragma once
/// \file temperature.h
/// \brief Header 
///
/// \class temperature
/// \brief ������������� ������� ����������
/// @author ����� ��������� ����������
class temperature
{

private:
	int first;
	int last;
	double ylinear[100];
	double ynonlinear[100];

public:
	// \brief ����������� ��� ����������
	temperature();
	/// \brief ����������� � �����������
	temperature(int First, int Last);
	/// \brief �������� �������
	void lineart(int start, int end);
	/// \brief ���������� �������
	void nonlineart(int start, int end);
	/// \brief ������� ������
	void print();
};