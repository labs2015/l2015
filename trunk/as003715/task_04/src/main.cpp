#include <iostream>
/// \file main.cpp
/// \brief Main file
#include <math.h>
#include "temperature.h"
using namespace std;

/// @brief Main function
/// @return 0

int main()
{
	setlocale(LC_ALL, "Rus");
	int s1 = 0;
	int s2 = 0;
	cout << "������� ������ � ����� ����������" << endl;
	cin >> s1 >> s2;

	temperature tt(s1, s2);
	tt.lineart(s1, s2);
	tt.nonlineart(s1, s2);
	tt.print();

	system("pause");

	return 0;
}