#pragma once
///@image html NonlinearGraphic.png
#include "GeneralModel.h"
///@brief �������� �����, ����������� ���������� ������
/**
������� �������� ���������� �.�. ������������ ��������, ������� ���������� ����������� ����� � ������.

����� �������, ������ ���������� ���������� ����������� ����� �� ������ �������.
*/
class NonlinearModel :
	public GeneralModel
{
private:
	double yt;
	double ut;
public:
	NonlinearModel();
	///@brief ������ ������� �������� ������������� � ��������� ������� �������� �����������
	///@param y0 - ��������� ������� �������� �����������
	///@param ut - ������� �������� �������������
	NonlinearModel(double y0, double ut);
	///@brief ������� �� ������� ������� �������� ����������� �� ������ ��������(���������� ������)
	///@param t - ���������� ��������
	void PrintModel(int t);
	~NonlinearModel();
};



