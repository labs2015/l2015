///@file main.cpp
///@author ������� ��������
///@13.05.2015
#include <iostream>
///@mainpage ������� ��������
///@image html LZ.png
///� ������� �������� ����������� ����������� ���������� �������, �.� � � ��������� ����������� ��������� ����������
///@image html NLZ.png
///�� ������� ���������� ����������� ����������� ���������� �� �������, �.� � � ��������� ���� ��������� ���������� 

using namespace std;

class Object
{
protected:
	double y0 = 0;

public:
	///@brief ���������� ��������� �������� �������� ����������� �������
	double getY0()
	{
		return y0;
	}

	///@brief ������������� ��������� �������� �������� ����������� �������
	void setY0(double y0)
	{
		this->y0 = y0;
	}

	///@brief ������� ��������� � �������� ����������� �������
	virtual void showTemperature(int(*u)(int)) = 0;
};

class Object1 : public Object
{
public:

	void showTemperature(int(*ut)(int))
	{
		cout.width(8);
		cout << "y" << "  |  " << "u" << endl;
		cout << "---------------" << endl;
		cout.width(8);
		cout << y0 << "  |  " << ut(0) << endl;
		double y = y0;
		for (int i = 1; i < 10; i++)
		{
			cout.width(8);
			cout << (y = 0.988*y + 0.232*ut(i - 1)) << "  |  " << ut(i - 1) << endl;
		}

		///@image html LZ.PNG
	}
};

class Object2 : public Object
{
protected:
	double y1 = 20;

public:

	///@brief ���������� ������ �������� �������� ����������� �������
	double getY1()
	{
		return y1;
	}

	///@brief ������������� ������ �������� �������� ����������� �������
	void setY1(double y1)
	{
		this->y1 = y1;
	}

	void showTemperature(int(*ut)(int))
	{
		cout.width(8);
		cout << "y" << "  |  " << "u" << endl;
		cout << "---------------" << endl;
		cout.width(8);
		cout << y0 << "  |  " << ut(0) << endl;
		cout.width(8);
		cout << y1 << "  |  " << ut(1) << endl;
		double yLast = y0, yNext = y1;
		for (int i = 2; i < 10; i++)
		{
			double y = yNext;
			cout.width(8);
			cout << (yNext = 0.9*yNext - 0.001*yLast*yLast + ut(i - 1) + sin(ut(i - 2))) << "  |  " << ut(i - 1) << endl;
			yLast = y;
		}
			
		///@image html NLZ.PNG
	}
};

///@brief ���������� �������� �����, ������������ � �������
int u(int t)
{
	return 50;
}

///@brief ������ ������������� ����������� �������
int main()
{
	Object1 o1;
	Object2 o2;
	o1.showTemperature(&u);
	cout << endl << endl;
	o2.showTemperature(&u);
	system("PAUSE");
}