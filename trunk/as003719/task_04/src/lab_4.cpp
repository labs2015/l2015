/// \file lab_4.cpp
/// \brief class temp file
#include "lab_4.h"
#include <math.h>
#include <iostream>
using namespace std;

temp::temp()
{
	{
		first = 0;
		last = 100;
	}
	for (int i = 0; i < 100; i++)
	{
		x[i] = 0;
		y[i] = 0;
	}
}

temp::temp(int First, int Last)
{
	{
		first = First;
		last = Last;
	}
	for (int i = first; i < last; i++)
	{
		x[i] = 0;
		y[i] = 0;
	}
}

void temp::line(int nOtr, int kOtr)
{
	x[nOtr] = 0.988 * 0 + 0.232 * 1;
	for (int i = nOtr + 1; i < kOtr; i++)
		x[i] = 0.988 * x[i - 1] + 0.232 * 1;
}

void temp::noline(int nOtr, int kOtr)
{
	y[nOtr] = 0.9 * 0 - 0.001 * 0 + 1 + sin(1);
	y[nOtr + 1] = 0.9 * y[nOtr] + 1 + sin(1);
	for (int i = nOtr + 2; i < kOtr; i++)
	{
		y[i] = 0.9 * y[i - 1] - 0.001 * pow((y[i - 2]), 2) + 1 + sin(1);
	}
}

 void temp::print()
{
	cout << "�������� ������: " << endl;
	for (int i = first; i < last; i++)
	{
		cout << "y=";
		cout << x[i] << endl;
	}
	cout << "���������� ������: " << endl;
	for (int i = first; i < last; i++)
	{
		cout << "y=";
		cout << y[i] << endl;
	}
	///@image html line.png
	///@image html noline.png
}