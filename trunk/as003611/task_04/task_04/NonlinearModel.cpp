#include "NonlinearModel.h"
#include<iostream>
#include<math.h>

NonlinearModel::NonlinearModel()
{
}

NonlinearModel::NonlinearModel(double y0, double ut)
{
	this->yt = y0;
	this->ut = ut;
}

void NonlinearModel::PrintModel(int t)
{
	int i = 0;
	double temp = yt;
	double yprev = 0;
	yt = 0.9*yt - 0.001*yprev*yprev + ut + sin(ut);
	yprev = temp;
	while (i < t)
	{
		std::cout << yt << std::endl;
		temp = yt;
		yt = 0.9*yt - 0.001*yprev*yprev + ut + sin(ut);
		yprev = temp;
		i++;
	}
};
NonlinearModel::~NonlinearModel()
{
}
