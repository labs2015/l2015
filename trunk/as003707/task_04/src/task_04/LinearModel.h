#pragma once
///@image html LinearGraphic.png
/**
������� �������� �������� �.�. ��� ��������� ������� ���������� ����������� ����� � ������.

����� �������, ������� ���������� �������� ����������� ����� �� ������ �������.
*/
#include "GeneralModel.h"
///@brief �������� �����, ����������� �������� ������
class LinearModel :
	public GeneralModel
{
private:
	double yt;
	double ut;
public:
	LinearModel();
	///@brief ������ ������� �������� ������������� � ��������� ������� �������� �����������
	///@param yt - ��������� ������� �������� �����������
	///@param ut - ������� �������� �������������
	LinearModel(double yt, double ut);
	///@brief ������� �� ������� ������� �������� ����������� �� ������ ��������(�������� ������)
	///@param t - ���������� ��������
	void PrintModel(int t);
	~LinearModel();
};
