/// @mainpage
/// @image html p1.png "������� 1 - �������� ������"
/// ������� �������� ��������, �.�.��� ��������� ������� ���������� ����������� ����� � ������
/// @image html p2.png "������� 2 - ���������� ������"
/// ������� �������� ����������, �.�.���� �������� ������� ���������� ����������� ����� � ������
/// @file main.cpp Object simulating, that change its temperature by the differential equation
/// @brief Code of warming object model on C++

/// @author Marchuk Viktoria

#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

/// @brief Function for the linear analysis of simulating object
/// 
/// This function simulating object, that change its temperature by
/// the differential equation. This model is linear
/// 
/// @param QuantPeriod (T) Quantization period of model. Must be greater that null
/// @param OutputTemperature y(t) Output temperature of model
/// @param OutputWarmth u(t) Output warmth. In this case it constant
///
/// @return Vector, which represents result of calculating linear model
vector<double> Linear(int QuantPeriod, double OutputTemperature, double OutputWarmth)
{
	vector<double> linearResults;
	linearResults.push_back(OutputTemperature);

	// Starts from second quantization time, because calculating of next value
	// based on last calculation
	for (int period = 1; period < QuantPeriod; period++)
	{
		linearResults.push_back(0.988 * linearResults.back() + 0.232 * OutputWarmth);
	}

	return linearResults;
}

/// @brief Function for the non-linear analysis of simulating object
/// 
/// This function simulating object, that change its temperature by
/// the differential equation. This model is non-linear
/// 
/// @param QuantPeriod (T) Quantization period of model. Must be greater that null
/// @param OutputTemperature y(t) Output temperature of model
/// @param OutputWarmth u(t) Output warmth. In this case it constant
///
/// @return Vector, which represents result of calculating non-linear model
vector <double> NonLinear(int QuantPeriod, double OutputTemperature, double OutputWarmth)
{
	vector<double> nonLinearResults;
	nonLinearResults.push_back(OutputTemperature);
	nonLinearResults.push_back(OutputTemperature);

	// Starts from third quantization time, because calculating of next value
	// based on two last calculations
	for (int period = 2; period < QuantPeriod; period++)
	{
		// Calculation with condition, that output warmth is constant
		nonLinearResults.push_back(0.9 * nonLinearResults[period - 1] - 0.001 * pow(nonLinearResults[period - 2], 2.0) + OutputWarmth + sin(OutputWarmth));
	}

	return nonLinearResults;
}

/// @brief The main function on project
///
/// It function represents input point for application starting
int main()
{
	int QuantPeriod = 0;
	double OuputTemperature = 0.0, OutputWarmth = 0.0;

	// Entering the source data for calculations
	cout << "Please, input quant period: ";
	cin >> QuantPeriod;
	cout << "Ouput temperature: ";
	cin >> OuputTemperature;
	cout << "Ouput warmth: ";
	cin >> OutputWarmth;

	vector<double> linearModelResults = Linear(QuantPeriod, OuputTemperature, OutputWarmth);
	vector<double> nonLinearModelResults = NonLinear(QuantPeriod, OuputTemperature, OutputWarmth);

	// Output the result table of calculations
	cout << " #" << "\t" << "Linear" << "\t\t" << "Non-linear" << endl;

	for (int i = 0; i < linearModelResults.size(); i++)
		cout << i + 1 << "\t" << linearModelResults[i] << "\t\t" << nonLinearModelResults[i] << endl;

	return 0;
}
