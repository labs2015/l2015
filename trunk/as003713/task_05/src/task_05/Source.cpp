/**
@file Source.cpp
@author �������� �. �. ��-37
@date 22.05.2015
*/
#include<iostream>
#include"GeneralModel.h"
#include"LinearModel.h"
#include"NonlinearModel.h"
#include"PIDRegulator.h"
#include<iostream>

using namespace std;
/**
@brief �������� ������������� ���-����������.
������� �� ������� �������� �����������

*/
void main()
{
	const double w = 75.0;

	LinearModel *linMod = new LinearModel();
	NonlinearModel *nonlinMod = new NonlinearModel(0);
	PIDRegulator *regul = new PIDRegulator(0.01, 0.5, 0.21, 0.5);

	double y = linMod->Model(0,0);
	double u = regul->Deviation(y, w);

	for (int i = 0; i < 200; i++)
	{
		y = linMod->Model(y, u);
		u = regul->Deviation(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}
	cout << "NonLinear Model: " << endl;
	
	y = nonlinMod->Model(0, 0);
	u = regul->Deviation(y, w);

	for (int i = 0; i < 200; i++)
	{
		y = nonlinMod->Model(y, u);
		u = regul->Deviation(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}

	system("PAUSE");
}