#pragma once
/**
@mainpage ������
@image html LinearGraphic.png
@image html NonlinearGraphic.png
�� �������� ����� ��� �� ���������� ������.
���������� ������ ������������ ������� ��� ��������.
*/
class GeneralModel
{
public:
	GeneralModel();
	///@brief ����������� ����� ��� ������ �������� �������
	virtual double Model(double yt, double ut) = 0;
	~GeneralModel();
};


