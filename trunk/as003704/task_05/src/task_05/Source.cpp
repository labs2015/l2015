///@file ���������� ���-���������
///@author ������ �.�.
///@date 28.05.2015

#include <iostream>
#include "THLinear.h"
#include "THNoLinear.h"
#include "THPID_Regulator.h"

using namespace std;


void main()
{
	const double w = 65;

	THLinear *ob_l = new THLinear();
	THNoLinear *ob_nl = new THNoLinear(0);
	THPID_Regulator *r = new THPID_Regulator(0.009, 0.5, 0.2, 0.5);

	cout << "Linear Model: \n" << endl;

	double y = ob_l->Show(0, 0);
	double u = r->Solution(y, w);

	for (int i = 0; i < 65; i++)
	{
		y = ob_l->Show(y, u);
		u = r->Solution(y, w);
		cout << "yt = " << y << " | " << "ut = " << u << endl;
	}
	cout << "NonLinear Model: \n" << endl;

	y = ob_nl->Show(0, 0);
	u = r->Solution(y, w);

	for (int i = 0; i < 65; i++)
	{
		y = ob_nl->Show(y, u);
		u = r->Solution(y, w);
		cout << "yt = " << y << " | " << "ut = " << u << endl;
	}

	system("PAUSE");
}