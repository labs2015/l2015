#include "THPID_Regulator.h"

THPID_Regulator::THPID_Regulator()
{}

THPID_Regulator::THPID_Regulator(double Td, double T, double T0, double K)
{
	this->Td = Td;
	this->T = T;
	this->T0 = T0;
	this->K = K;

	q0 = K * (1 + (Td / T0));
	q1 = -K *(1 + 2 * (Td / T0) - (T0 / T));
	q2 = K * (Td / T0);
	u = 0;
	e1 = 0;
	e2 = 0;
	e3 = 0;
}

/// @brief �����, ������������� u(t)

double THPID_Regulator::Solution(double y, double w)

{
	e1 = e2;
	e2 = e2;
	e1 = w - y;
	u += (q0 * e1 + q1 * e2 + q2 * e3);
	return u;
}

THPID_Regulator::~THPID_Regulator()
{}