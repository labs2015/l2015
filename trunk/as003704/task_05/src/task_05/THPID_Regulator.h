/// @brief ����� PID_regulator, ������������ �������� ����������� 

class THPID_Regulator
{
private:
	double q0, q1, q2, T0, Td, T, K, e1, e2, e3, u;

public:
	THPID_Regulator();
	THPID_Regulator(double Td, double T0, double T, double K);

	double Solution(double y, double w);
	~THPID_Regulator();
};

