#include "THAbstractClass.h"

/// @brief ��������� ����������� ������ "THAbstractClass", ����������� �������� ������

class THNoLinear_function : THAbstractClass {
private:
	static const int size = 100;

	bool make_funk;
	double *massWithFuncValues;

	double y;
	int t;
	int T;
	double u;

	int numInterval;

public:

	void showFunc();

	void func(int t, int T, double y, double u);

	double* getY_valueFunction();

	THNoLinear_function();
	~THNoLinear_function();
};