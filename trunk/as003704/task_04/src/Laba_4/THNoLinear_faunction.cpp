
#include "THNoLinear_function.h"
#include <iostream>
using namespace std;

THNoLinear_function::THNoLinear_function() {
	this->massWithFuncValues = new double[size];
	this->make_funk = false;
}

/// @brief ������ �������
double* THNoLinear_function::getY_valueFunction() {
	if (make_funk){
		return this->massWithFuncValues;
	}
	else {
		cout << "�������� ������� �������";
		return NULL;
	}
}

/// @brief ������� ��� ������ ��������
void THNoLinear_function::showFunc() {
	if (make_funk){
		for (int i = 0; i < size; i++){
			cout << "Y(t+1)=" << this->massWithFuncValues[i] << endl;
			if (i == numInterval){
				break;
			}
		}
	}
	else {
		cout << "�������� ������� �������";
	}
}


void THNoLinear_function::func(int t, int T, double y, double u) {

	/// @brief "interval" - ����� ��������, ������������ �� ��������� t/T

	this->t = t;
	this->T = T;
	this->u = u;
	this->y = y;
	
	int interval = t / T;
	double func = 0, last = 0;
	for (int i = 0; i < interval; i++){
		func = 0.9*y - 0.001*last*last + u + sin(u);
		last = y;
		y = func;
		if (i < size) {
			massWithFuncValues[i] = func;
		}
		this->numInterval = i;
	}

	this->make_funk = true;
}

THNoLinear_function::~THNoLinear_function(){
	delete[]massWithFuncValues;
}
