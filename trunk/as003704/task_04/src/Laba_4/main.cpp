#include <iostream>
#include "THLinear_function.h"
#include "THNoLinear_function.h"
using namespace std;

void main(){
	/// \brief main ������ ���������
	
	cout << "Linear Function" << endl;
	THLinear_function lineF;
	lineF.func(20, 1, 1, 1);
	lineF.showFunc();

	cout << "NoLinear Function" << endl;
	THNoLinear_function nolineF;
	nolineF.func(20, 1, 1, 1);
	nolineF.showFunc();

	cin.get();
}