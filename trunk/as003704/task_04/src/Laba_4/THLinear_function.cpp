
#include "THLinear_function.h"
#include <iostream>
using namespace std;

THLinear_function::THLinear_function() {
	this->massWithFuncValues = new double[size];
	this->make_funk = false;
}

/// @brief ������ �������
double* THLinear_function::getY_valueFunction() {
	if (make_funk){
		return this->massWithFuncValues;
	}
	else {
		cout << "�������� ������� �������";
		return NULL;
	}
}

/// @brief ������� ��� ������ ��������
void THLinear_function::showFunc() {
	if (make_funk){
		for (int i = 0; i < size; i++){
			cout << "Y(t+1)=" << this->massWithFuncValues[i] << endl;
			if (i == numInterval){
				break;
			}
		}
	}
	else {
		cout << "�������� ������� �������";
	}
}


void THLinear_function::func(int t, int T, double y, double u) {

	/// @brief "interval" - ����� ��������, ������������ �� ��������� t/T

	this->t = t;
	this->T = T;
	this->u = u;
	this->y = y;

	int interval = t / T;
	for (int i = 0; i < interval; i++){
		y = 0.988*y + 0.232*u;
		
		if (i < size) {
			massWithFuncValues[i] = y;
		}
		this->numInterval = i;
	}

	this->make_funk = true;
}

THLinear_function::~THLinear_function(){
	delete[]massWithFuncValues;
}
