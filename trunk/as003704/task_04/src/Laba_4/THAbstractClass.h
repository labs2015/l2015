#pragma once
/// @brief ������������ �����

class THAbstractClass {

public:

	/// @brief ����������� ����� ������������ ��� ���������� ��������� ������� 

	/// @param "t" - ���������� ������������� �����
	/// @param "T" - �������� �����������
	/// @param "y" - �������� �����������, ����������� � �������
	/// @param "u" - �������� �����, ����������� � �������

	virtual void func(int t, int T, double y, double u) = 0;

	THAbstractClass() {
	}

	~THAbstractClass() {
	}
};

