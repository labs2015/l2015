#pragma once
/// \brief �����, ������������ ������ PID-����������.

class PID_Regulator
{
private:
	double T;
	double Td;
	double T0;
	double K;
	double q0;
	double q1;
	double q2;
	
	
	double ut;

	double et;
	double et_1;
	double et_2;
public:
	PID_Regulator(double _Td,double _T,double _T0,double _K);
	double calculate(double _wt,double _yt);
	~PID_Regulator();
};

