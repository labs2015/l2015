/*!
\author ���������� ������� ��-36
\date 26.05.2015
*/
/*!
\mainpage ������ ������ ���-����������
\image html linear.png "���.1 �������� ������."
\image html nonlinear.png "���.2 ���������� ������."
����� ��������, ��� ���������� ������ �������� ������� ��� ��������.
*/
#include<iostream>
#include"linear_model.h"
#include"nonlinear_model.h"
#include"PID_Regulator.h"
#include<iomanip>
#include<fstream>

using namespace std;
/// \brief ������� ���������� ������ ���-���������� � ��, �������� ���������� �������� ���������� �� ��������� ������.

void linear_behavior()
{
	ofstream f;
	f.open("linear.txt");
	
	cout << "Linear model:" << endl;
	double W;
	cout << "Please, introduce W:"; cin >> W;
	double Td = 0.02, T = 0.45, T0 = 0.17, K = 0.6;
	linear_model ln;

	PID_Regulator reg(Td, T, T0, K);

	double yt_1 = 0;
	double yt = 0, ut = 0;
	yt = ln.temperature(yt_1, 0);
	ut = reg.calculate(W, yt);
	yt_1 = yt;
	
	cout  << "y(" << 1 << ") = " << yt << ";" << " u(" << 1 << ") = " << yt << endl;
	for (int i = 0; i < 200; i++)
	{
		yt = ln.temperature(yt_1, ut);
		ut = reg.calculate(W, yt);
		yt_1 = yt;
		cout  << "y(" << i + 2 << ") = " << yt << "; "  << "u(" << i + 2 << ") = " << ut << endl;
		f << yt;
		f << '\n';
	}

}
/// \brief ������� ���������� ������ ���-���������� � ��, �������� ���������� �������� ���������� �� ����������� ������.
void nonlinear_behavior()
{
	ofstream f;
	f.open("nonlinear.txt");
	cout << "nonlinear model:" << endl;
	double W;
	cout << "Please, introduce W:"; cin >> W;
	double Td = 0.02, T = 0.45, T0 = 0.17, K = 0.6;
	nonlinear_model nonln(0, 0);
	PID_Regulator reg2(Td, T, T0, K);
	double yt_1 = 0;
	double yt = 0, ut = 0;
	yt = nonln.temperature(0, 0);

	ut = reg2.calculate(W, yt);
	yt_1 = yt;

	cout << "y(" << 1 << ") = " << yt << ";" << " u(" << 1 << ") = " << ut << endl;
	for (int i = 0; i < 200; i++)
	{
		yt = nonln.temperature(yt_1, ut);
		ut = reg2.calculate(W, yt);
		yt_1 = yt;
		cout << "y(" << i + 2 << ") = " << yt << ";" << " u(" << i + 2 << ") = " << ut << endl;
		f << yt;
		f << '\n';
	}
}
/// \brief ������� ����� � ���������.

int main()
{
	linear_behavior();
	system("pause");
	nonlinear_behavior();
	system("pause");
	return 0;	
}