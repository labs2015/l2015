﻿///@file Моделирует объект управления
///@author Николаенко А.А.
///@date 14.05.2015
#include<iostream>
#include<math.h>

using namespace std;

/// \brief Базовый абстрактный класс Model

class Model
{

public: Model();
		/// \brief Функция Show выводит информацию о выходной температуре 
		virtual void Show(int ut) = 0;
		~Model();

};

Model::Model()
{}

Model::~Model()
{}

/// \brief Класс Linear_model, который наследуется от класса Model
/// \details Реализует линейную модель поведения объекта
class Linear_model : public Model
{

private: 
	double yt;

public:
	Linear_model();
	Linear_model(double yt);
	void Show(int ut);
	~Linear_model();

};

Linear_model::Linear_model()
{}

Linear_model::Linear_model(double yt)
{
	this->yt = yt;
}

void Linear_model::Show(int ut)
{
	cout << "Linear model" << endl;
	for (int i = 0; i < 10; i++)
	{
		yt = 0.988*yt + 0.232*ut;
		cout << yt << endl;
	}
	///@image html D:\linear_model.png
	/*!
	\brief График линейного изменения температуры объекта

	Система является линейной, т.к. нет элементов имеющих нелинейную зависимость входа и выхода 
	*/
}

Linear_model::~Linear_model()
{}

/// \brief Класс Nonlinear_model, который наследуется от класса Model
/// \details Реализует нелинейную модель поведения объекта

class Nonlinear_model : public Model
{

private: 
	double yt;

public:
	Nonlinear_model();
	Nonlinear_model(double y0);
	void Show(int ut);
	~Nonlinear_model();

};

Nonlinear_model::Nonlinear_model()
{}

Nonlinear_model::Nonlinear_model(double y0)
{
	this->yt = y0;
}

void Nonlinear_model::Show(int ut)
{
	double y = yt;
	double ylast = 0;
	yt = 0.9*yt - 0.001*pow(ylast, 2) + ut + sin(ut);
	ylast = y;
	cout << endl;
	cout << "Nonlinear model" << endl;
	for (int i = 0; i < 10; i++)
	{
		cout << yt << endl;
		y = yt;
		yt = 0.9*yt - 0.001*pow(ylast, 2) + ut + sin(ut);
		ylast = y;
	}
	///@image html D:\nonlinear_model.png
	/*!
	\brief График нелинейного изменения температуры объекта

	Система является нелинейной, т.к. есть параметр имеющий нелинейную зависимость входа и выхода
	*/
}

Nonlinear_model::~Nonlinear_model()
{}

void main()
{
	Linear_model ob1(100);
	Nonlinear_model ob2(100);
	ob1.Show(125);
	ob2.Show(125);
	system("pause");
}