#include "PIDController.h"


PIDController::PIDController(double _T, double _T0, double _Td, double _K) :T(_T), T0(_T0), Td(_Td), K(_K)
{
	u = 0;
	e = e_1 = e_2 = 0;

	q0 = K * (1 + (Td / T0));
	q1 = -K *(1 + 2 * (Td / T0) - (T0 / T));
	q2 = K * (Td / T0);
}

double PIDController::Calculate_u(double _w, double _y)
{
	e_2 = e_1;
	e_1 = e;
	e = _w - _y;
	u += (q0 * e + q1 * e_1 + q2 * e_2);
	return u;
}


PIDController::~PIDController()
{
}
