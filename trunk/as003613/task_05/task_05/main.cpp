/*!
\author �������� ��������� ��-36
\date 24.05.2015
*/
/*!
\mainpage ������ � �������������� ���-����������
\image html LinearModel.png
\image html NonLinearModel.png
�� �������� �����, ��� ���������� ������ �������� ������� ��� ��������
*/

#include <iostream>
#include <string>
#include <fstream>
#include "LinearModel.h"
#include "NonlinearModel.h"
#include "PIDController.h"
using namespace std;

void linearModel()
{
	LinearModel lModel;
	PIDController controller(0.5, 0.21, 0.01,0.5);
	double w;
	cout << "������� w:" << endl;
	cin >> w;
	double y = lModel.Modeling(0, 0);
	double u = controller.Calculate_u(w, y);
	ofstream f;
	f.open("LinearModel.txt");
	for (int i = 0; i < 150; i++)
	{
		f << y << endl;
		cout << "y = " << y << "\t" << "u = " << u << endl;
		y = lModel.Modeling(y, u);
		u = controller.Calculate_u(w, y);
	}
	f.close();
}

void nonlinearModel()
{
	NonlinearModel nModel(0, 0);
	PIDController controller(0.5, 0.21, 0.01, 0.5);
	double w;
	cout << "������� w:" << endl;
	cin >> w;
	double y = nModel.Modeling(0, 0);
	double u = controller.Calculate_u(w, y);
	ofstream f;
	f.open("NonlinearModel.txt");
	for (int i = 0; i < 150; i++)
	{
		f << y << endl;
		cout << "y = " << y << "\t" << "u = " << u << endl;
		y = nModel.Modeling(y, u);
		u = controller.Calculate_u(w, y);
	}
	f.close();
}

int main()
{
	setlocale(LC_ALL, "Russian");
	while (true)
	{
		string answer;
		cout << "������ �������:" << endl;
		cout << "1. �������� ������" << endl;
		cout << "2. ���������� ������" << endl;
		cout << "�������� ����� ������:" << endl;
		cin >> answer;
		if (answer == "1")
			linearModel();
		if (answer == "2")
			nonlinearModel();
		if (answer != "1" && answer != "2")
			cout << "error" << endl;

		cout << "������ ���������? (y/n)" << endl;
		
		cin >> answer;
		if (answer != "y")
			break;
	}
	
	system("pause");
	return 0;
}

