#pragma once
///�����, ����������� ���-���������
class PIDController
{
private:
	double q0, q1, q2;
	double T, T0, Td;
	double e, e_1, e_2;
	double K;
	double u;
public:
	PIDController(double _T, double _T0, double _Td, double _K);
	/*!
	����������� u(t)
	\param _w - �������� ���������������� �������
	\param _y - �������� ����������
	\return u(t) -  ����������� �����������
	*/
	double Calculate_u(double _w, double _y);
	~PIDController();
};

