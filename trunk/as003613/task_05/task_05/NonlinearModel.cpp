#include "NonlinearModel.h"

NonlinearModel::NonlinearModel(double _y0, double _u0)
{
	y_1 = _y0;
	u_1 = _u0;
}

double NonlinearModel::Modeling(double _y, double _u)
{
	double y = 0.9*_y - 0.001*y_1*y_1 + _u + sin(u_1);
	y_1 = _y;
	u_1 = _u;
	return y;
}
