/*!
\author �������� ��������� ��-36
\date 13.05.2015
*/
#include <iostream>
#include <string>
#include "LinearModel.h"
#include "NonlinearModel.h"
using namespace std;

void linearModel()
{
	LinearModel lModel;
	int time, T;
	double u, y;
	cout << "������� ������������� �����:" << endl;
	cin >> time;
	cout << "������� �������� �����������:" << endl;
	cin >> T;
	cout << "������� ����������� ����������� � �������:" << endl;
	cin >> y;
	cout << "������� ����� ����������� � �������:" << endl;
	cin >> u;
	lModel.Modeling(time, T, y, u);
}

void nonlinearModel()
{
	NonlinearModel nModel;
	int time, T;
	double u, y;
	cout << "������� ������������� �����:" << endl;
	cin >> time;
	cout << "������� �������� �����������:" << endl;
	cin >> T;
	cout << "������� ����������� ����������� � �������:" << endl;
	cin >> y;
	cout << "������� ����� ����������� � �������:" << endl;
	cin >> u;
	nModel.Modeling(time, T, y, u);
}

int main()
{
	setlocale(LC_ALL, "Russian");
	while (true)
	{
		string answer;
		cout << "������ �������:" << endl;
		cout << "1. �������� ������" << endl;
		cout << "2. ���������� ������" << endl;
		cout << "�������� ����� ������:" << endl;
		cin >> answer;
		if (answer == "1")
			linearModel();
		if (answer == "2")
			nonlinearModel();
		if (answer != "1" && answer != "2")
			cout << "error" << endl;

		cout << "������ ���������? (y/n)" << endl;
		
		cin >> answer;
		if (answer != "y")
			break;
	}
	
	system("pause");
	return 0;
}

