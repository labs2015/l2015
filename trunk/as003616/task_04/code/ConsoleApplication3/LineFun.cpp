#include "LineFun.h"
#include <iostream>
using namespace std;

LineFun::LineFun()
{
}
/// @param "t" - ���������� ������������� �����
/// @param "T" - �������� �����������
/// @param "y" - �������� ����������� ����������� � �������
/// @param "u" - �������� ����� ����������� � �������
/// \brief ����� �������� ������������ �� ��������� t/T
void LineFun::line(int t, int T, double y, double u){
	int interv = t / T;
	double fun = 0;
	for (int i = 0; i < interv; i++){
		fun = 0.988*y + 0.232*u;
		y = fun;
		cout << "Y(t+1)=" << fun << endl;
		/// @image html D:\Line.png
	}
}

LineFun::~LineFun()
{
}
