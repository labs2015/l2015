#pragma once
#include "MyAbstractClass.h"
/// \brief ��������� ����������� ������ "MyAbstractClass", ����������� �������� ������ ���������.
class LineFun :
	public MyAbstractClass
{
public:
	/// \brief ���������� ������ line ������������� ������ MyAbstractClass.
	/// \details ����� ���������� ��������� �������� ����������� ��� �������� ������� ������� 
	void line(int t, int T, double y, double u);
	LineFun();
	~LineFun();
};

