#pragma once
#include "MyAbstractClass.h"
/// \brief ��������� ����������� ������ "MyAbstractClass", ����������� ���������� ������ ���������.
class NoLineFun :
	public MyAbstractClass
{
public:
	/// \brief ���������� ������ line ������������� ������ MyAbstractClass.
	/// \details ����� ���������� ��������� �������� ����������� ��� �������� ������� ������� 
	void line(int t, int T, double y, double u);
	NoLineFun();
	~NoLineFun();
};

