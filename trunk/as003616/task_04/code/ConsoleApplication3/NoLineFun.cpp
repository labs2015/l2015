#include "NoLineFun.h"
#include <iostream>
#include <cmath>
using namespace std;

NoLineFun::NoLineFun()
{
}
/// @param "t" - ���������� ������������� �����
/// @param "T" - �������� �����������
/// @param "y" - �������� ����������� ����������� � �������
/// @param "u" - �������� ����� ����������� � �������
/// \brief ����� �������� ������������ �� ��������� t/T

void NoLineFun::line(int t, int T, double y, double u){
	int interv = t / T;
	double fun = 0;
	double pred = 0;
	for (int i = 0; i < interv; i++){
		fun = 0.9*y-0.001*pow(pred,2)+u+sin(u);
		pred = y;
		y = fun;
		cout << "Y(t+1)=" << fun << endl;
		/// @image html D:\NoLine.png
	}
}


NoLineFun::~NoLineFun()
{
}
