﻿///@file Моделирует ПИД-регулятор
///@author Потапюк А.Ю.
///@date 27.05.2015
#include<iostream>
#include<math.h>

using namespace std;

/// @brief Базовый абстрактный класс Model

class Model
{
public:Model();
	   virtual double Show(double yt, double ut) = 0;
	   ~Model();
};

Model::Model()
{}

Model::~Model()
{}

/// @brief Класс Linear_model, который наследуется от класса Model
/// @details Реализует линейную модель поведения объекта

class Linear_model : public Model
{
public:
	Linear_model();

	double Show(double yt, double ut);
	~Linear_model();
};

Linear_model::Linear_model()
{}

/// @brief Выводит на консоль входное значение температуры 

double Linear_model::Show(double yt, double ut)
{
	return 0.988*yt + 0.232*ut;
};

Linear_model::~Linear_model()
{}

/// @brief Класс Nonlinear_model, который наследуется от класса Model
/// @details Реализует нелинейную модель поведения объекта

class Nonlinear_model :
	public Model
{
private:
	double ylast;
public:
	Nonlinear_model();

	Nonlinear_model(double y0);

	double Show(double yt, double ut);
	~Nonlinear_model();
};

Nonlinear_model::Nonlinear_model()
{}

Nonlinear_model::Nonlinear_model(double y0)
{
	this->ylast = y0;
}

/// @brief Выводит на консоль входное значение температуры 

double Nonlinear_model::Show(double yt, double ut)
{
	double y_t = 0;
	y_t = 0.9*yt - 0.001*pow(ylast, 2) + ut + sin(ut);
	ylast = yt;
	return y_t;
};

Nonlinear_model::~Nonlinear_model()
{}

/// @brief Класс PID_regulator, регулирующий выходную температуру 

class PID_regulator
{
private:
	double q0, q1, q2, T0, Td, T, K, e1, e2, e3, u;

public:
	PID_regulator();
	PID_regulator(double Td, double T0, double T, double K);

	double Solution(double y, double w);
	~PID_regulator();
};

PID_regulator::PID_regulator()
{}

PID_regulator::PID_regulator(double Td, double T, double T0, double K)
{
	this->Td = Td;
	this->T = T;
	this->T0 = T0;
	this->K = K;

	q0 = K * (1 + (Td / T0));
	q1 = -K *(1 + 2 * (Td / T0) - (T0 / T));
	q2 = K * (Td / T0);
	u = 0;
	e1 = 0;
	e2 = 0;
	e3 = 0;
}

/// @brief Метод, высчитывающий u(t)

double PID_regulator::Solution(double y, double w)

{
	e1 = e2;
	e2 = e2;
	e1 = w - y;
	u += (q0 * e1 + q1 * e2 + q2 * e3);
	return u;
}

PID_regulator::~PID_regulator()
{}


void main()
{
	const double w = 65;

	Linear_model *ob_l = new Linear_model();
	Nonlinear_model *ob_n = new Nonlinear_model(0);
	PID_regulator *r = new PID_regulator(0.009, 0.5, 0.2, 0.5);

	cout << "Linear Model: \n" << endl;

	double y = ob_l->Show(0, 0);
	double u = r->Solution(y, w);

	for (int i = 0; i < 65; i++)
	{
		y = ob_l->Show(y, u);
		u = r->Solution(y, w);
		cout << "yt = " << y << " | " << "ut = " << u << endl;
	}
	cout << "NonLinear Model: \n" << endl;

	y = ob_n->Show(0, 0);
	u = r->Solution(y, w);

	for (int i = 0; i < 65; i++)
	{
		y = ob_n->Show(y, u);
		u = r->Solution(y, w);
		cout << "yt = " << y << " | " << "ut = " << u << endl;
	}

	system("PAUSE");
}