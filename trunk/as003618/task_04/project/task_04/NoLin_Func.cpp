#include "NoLin_Func.h"
#include <iostream>
#include <cmath>
using namespace std;

NoLin_Func::NoLin_Func()
{
}
/// @param "t" - ���������� ������������� �����
/// @param "T" - �������� �����������
/// @param "y" - �������� �����������, ����������� � �������
/// @param "u" - �������� ����� ����������� � �������
/// @brief "interval" ����� ��������, ������������ �� ��������� t/T

void NoLin_Func::Funct(int t, int T, double y, double u){
	int interval = t / T;
	double func = 0,last=0;
	for (int i = 0; i < interval; i++){
		func = 0.9*y-0.001*pow(last,2)+u+sin(u);
		last = y;
		y = func;
		cout << "Y"<<(t+1)<<"=" << y << endl;
	}
}


NoLin_Func::~NoLin_Func()
{
}
