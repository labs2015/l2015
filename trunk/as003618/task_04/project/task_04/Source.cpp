/// @file Source.cpp
/// @author ������� �.�.
#include "Lin_Func.h"
#include "NoLin_Func.h"
#include <iostream>
using namespace std;
void main(){
	/// \brief main ��������� ������ ����� � ��������� 
	/// \brief ����� �������� ������ ������� ����������. 
	cout << "Line Function" << endl;
	Lin_Func line;
	line.Funct(15, 1, 1, 1);
	/// \brief ����� ���������� ������ ������� ����������. 
	cout << "NoLine Function" << endl;
	NoLin_Func noline;
	noline.Funct(15, 1, 1, 1);
	cin.get();
}