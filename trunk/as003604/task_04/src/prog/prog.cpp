///@file
///@author ������� ���������
///@19.05.2015
#include <iostream>

using namespace std;

class ObjectManage
{
public:

	double y0 = 0;

	///@brief ������� ��������� � �������� �����������
	virtual void Temperature(int u) = 0;
};

class Linear : public ObjectManage
{
public:


	void Temperature(int u)
	{
		double y;
		y = y0;

		cout.width(10);
		cout << "value of y" << "  |  " << "value of u" << endl<<endl;

		cout.width(10);
		cout << y0 << "  |  " << u << endl;
		
		for (int i = 1; i < 20; i++)
		{
			cout.width(10);
			cout << (y = 0.988*y + 0.232*u) << "  |  " << u << endl;
		}

	}
};

class NotLinear : public ObjectManage
{

public:

	double y1 = 0;

	void Temperature(int u)
	{
		double yL, yN, yC;
		yL = y0;
		yN = y1;

		cout.width(10);
		cout << "value of y" << "  |  " << "value of u" << endl << endl;

		cout.width(10);
		cout << y0 << "  |  " << u << endl;
		cout.width(10);
		cout << y1 << "  |  " << u << endl;

		for (int i = 2; i < 20; i++)
		{
			yC = yN;
			cout.width(10);
			cout << (yN = 0.9*yN - 0.001*yL*yL + u + sin(u)) << "  |  " << u << endl;
			yL = yC;
		}

	}
};

///@mainpage ������
///@image html pN.png
/// \details ����������� �� ������� �������� ��������, ��������� � � ��������� ����������� ��������� ����������
/// \details ����������� �������������� �� ������� �������� ��������, ��������� � � ��������� ���� ��������� ����������


int main()
{
	Linear one;
	NotLinear two;
	one.Temperature(50);
	cout << endl << endl <<endl << endl;
	two.Temperature(50);

	system("pause");
}