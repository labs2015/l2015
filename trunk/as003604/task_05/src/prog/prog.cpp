///@file
///@author ������� ���������
///@26.05.2015
#include <iostream>

using namespace std;

class ObjectManage
{
public:
	double y0 = 0;

	///@brief ������� ��������� � �������� �����������
	virtual double Temperature(double y, double u) = 0;
};

class Linear : public ObjectManage
{
public:


	double Temperature(double y, double u)
	{
		return 0.988*y + 0.232*u;
	}
};


class NotLinear : public ObjectManage
{
protected:
	double yL = 20;

public:

	///@brief ���������� �������� �������� ����������� �������
	double getY1()
	{
		return yL;
	}

	///@brief ������������� �������� �������� ����������� �������
	void setY1(double y)
	{
		this->yL = y;
	}

	double Temperature(double y, double u)
	{
		double result = 0.9*y - 0.001*yL*yL + u + sin(u);
		yL = y;
		return result;
	}
};

///@mainpage ������
///@image html P.png
///@brief ���������� ������ ������������ ������� ��� ��������



class PID
{
private:
	double T0, Td, T, k;
	double q0, q1, q2, e1 = 0, e2 = 0, e3 = 0, u = 0;
public:

	///@brief ������������ �������� � ������������ �������� q
	PID(double T0, double Td, double T, double k)
	{
		this->T0 = T0;
		this->Td = Td;
		this->T = T;
		this->k = k;

		q0 = k * (1 + (Td / T0));
		q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = k * (Td / T0);
	}


	double Colculation(double y, double w)
	{
		e3 = e2;
		e2 = e1;
		e1 = w - y;
		u += (q0 * e1 + q1 * e2 + q2 * e3);
		return u;
	}

};

///@brief ������ ������������� ����������� �������
int main()
{
	Linear l;
	NotLinear nl;

	PID pid(0.2, 0.01, 0.5, 0.5);

	nl.setY1(0);

	double w = 60, y = l.Temperature(0, 0), u = pid.Colculation(y, w);

	cout << "Linear model" << endl << endl;


	for (int i = 0; i < 50; i++)
	{
		y = l.Temperature(y, u);
		u = pid.Colculation(y, w);
		cout.width(8);
		cout << y << "  |  " << u << endl;
	}

	cout << endl << endl;


	cout << "Not linear model" << endl << endl;

	y = nl.Temperature(0, 0);
	u = pid.Colculation(y, w);

	for (int i = 0; i < 50; i++)
	{
		y = nl.Temperature(y, u);
		u = pid.Colculation(y, w);
		cout.width(8);
		cout << y << "  |  " << u << endl;
	}

	system("Pause");
}