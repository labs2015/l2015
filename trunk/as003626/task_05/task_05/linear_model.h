#pragma once
#include "Model.h"
/// \brief ����� ����������� �� ������������ ������ Model. 
/// \details ������ ����� ����� ������ ���� �����, ������������ �������� ����������� ����������� �� �������.
class linear_model : public Model
{
public:
	linear_model();

	/// \brief ����� ���������� �������� ������ ���������.

	double temperature(double _Yt, double _Ut);
	~linear_model();
};
