#pragma once
/// \brief �����, ������������ ������ PID-����������.

class PID_Regulator
{
private:
	double T, Td, T0, K, q0, q1, q2, Ut, et, et_1, et_2;
	
public:
	PID_Regulator(double,double,double,double);
	double calculate(double,double);
	~PID_Regulator();
};

