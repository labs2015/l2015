#pragma once
#include "Model.h"
/// \brief ����� ����������� �� ������������ ������ Model. 
/// \details ������ ����� ����� ������ ���� �����, ������������ ���������� ����������� ����������� �� �������.
class nonlinear_model :
	public Model

{
private:
	double Yt_1, Ut_1;
public:
	nonlinear_model(double,double);

	///\brief ����� ���������� ���������� ������ ���������.

	double temperature(double _Yt, double _Ut);
	~nonlinear_model();
};

