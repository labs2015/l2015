///@file
///@author �������� �����
#include <iostream>

using namespace std;

class AbstractObject
{
protected:
	double y0 = 0;

public:
	///@brief ���������� ��������� �������� �������� t
	double getY0()
	{
		return y0;
	}

	///@brief ������������� ��������� �������� �������� t
	void setY0(double y0)
	{
		this->y0 = y0;
	}

	///@brief ������� ��������� � �������� t
	virtual void showT(int(*u)(int)) = 0;
};

class Object1 : public AbstractObject
{
public:

	void showT(int(*ut)(int))
	{
		
		cout << y0 << "\t\t" << ut(0) << endl;
		double y = y0;
		for (int i = 1; i < 10; i++)
		{
			cout.width(8);
			cout << (y = 0.988*y + 0.232*ut(i - 1)) << "  |  " << ut(i - 1) << endl;
		}

	}
};

class Object2 : public AbstractObject
{
protected:
	double y1 = 20;

public:

	///@brief ���������� ������ �������� �������� t
	double getY1()
	{
		return y1;
	}

	///@brief ������������� ������ �������� �������� t
	void setY1(double y1)
	{
		this->y1 = y1;
	}

	void showT(int(*ut)(int))
	{
		
		cout << y0 << "\t\t" << ut(0) << endl;
		cout << y1 << "\t\t" << ut(1) << endl;
		double yLast = y0, yNext = y1;
		for (int i = 2; i < 10; i++)
		{
			double y = yNext;
			cout.width(8);
			cout << (yNext = 0.9*yNext - 0.001*yLast*yLast + ut(i - 1) + sin(ut(i - 2))) << "  |  " << ut(i - 1) << endl;
			yLast = y;
		}

	}
};

int u(int t)
{
	return 100;
}


int main()
{
	Object1 o1;
	Object2 o2;
	o1.showT(&u);
	cout << endl << endl;
	o2.showT(&u);
}