﻿/**
@file
@date 26.03.2015
@author Навросюк Константин
@details hello world.cpp содержит функцию, которая выводит строку в консоль.
*/

#include <iostream>

using namespace std;
/** @brief Выводит строку в консоль*/
int main() {
	cout << "Hello, world!" << endl;
	system("pause");
	return 0;
}