﻿#pragma once
/**
@mainpage Модель
@image html Graphics.png
Из графиков видно, что модели регулируются.

Нелинейная модель регулируется быстрее чем линейная.
*/
class AbstractModel
{
public:
	AbstractModel();
	///@brief абстрактный метод для вывода значений моделей
	virtual double Run(double yt, double ut) = 0;
	~AbstractModel();
};


