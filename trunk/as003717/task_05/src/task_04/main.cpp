﻿/**
@file main.cpp
@author Навросюк К.С.
@date 25.05.2015
*/
#include<iostream>
#include"AbstractModel.h"
#include"LinearModel.h"
#include"NonlinearModel.h"
#include"PIDController.h"


using namespace std;
/**
@brief Содержит модель ПИД-регулятора.
Выводит на консоль выходную температуру
*/
void main(){
	const double w = 75.0;
	const int n = 120;

	LinearModel *linMod = new LinearModel();
	NonlinearModel *nonlinMod = new NonlinearModel(0);
	PIDController *regul = new PIDController(0.01, 0.5, 0.21, 0.5);

	double y = linMod->Run(0,0);
	double u = regul->Deviation(y, w);

	cout << "Linear Model: " << endl;
	for (int i = 0; i < n; i++){
		y = linMod->Run(y, u);
		u = regul->Deviation(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}
	cout << "NonLinear Model: " << endl;
	
	y = nonlinMod->Run(0, 0);
	u = regul->Deviation(y, w);

	for (int i = 0; i < n; i++){
		y = nonlinMod->Run(y, u);
		u = regul->Deviation(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}

	system("PAUSE");
}