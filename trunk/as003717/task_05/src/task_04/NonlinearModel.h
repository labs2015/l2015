﻿#pragma once
#include "AbstractModel.h"
class NonlinearModel :
	public AbstractModel
{
private:
	double ytPrev;
public:
	NonlinearModel();
	///@brief задает входное значение интенсивтости и начальное входное значение температуры
	///@param y0 - начальное входное значение температуры
	NonlinearModel(double y0);
	///@brief возвращает входное значение температуры на каждой итерации(Нелинейная модель)
	///@param yt - начальное входное значение температуры
	///@param ut - входное значение интенсивтости
	double Run(double yt, double ut);
	~NonlinearModel();
};



