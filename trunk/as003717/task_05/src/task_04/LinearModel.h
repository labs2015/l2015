﻿#pragma once
#include "AbstractModel.h"
///@brief дочерний класс, реализующий линейную модель
class LinearModel :
	public AbstractModel
{
public:
	LinearModel();
	///@brief возвращает входное значение температуры на каждой итерации(Линейная модель)
	///@param yt - начальное входное значение температуры
	///@param ut - входное значение интенсивтости
	///@return yt 
	double Run(double yt, double ut);
	~LinearModel();
};
