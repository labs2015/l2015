﻿#pragma once
/// Класс, регулирующий выходную температуру
class PIDController
{
private:
	double q0, q1, q2;
	double T0, Td, T;
	double K;
	double eFirst, eSec, eThird;
	double u;

public:
	PIDController();
	PIDController(double Td, double T0, double T, double K);
	/// высчитывает ut
	///@param y -  управляющее возздействие
	///@param w - алгоритм функционирования системы
	///@return - ut
	double Deviation(double y, double w);
	~PIDController();
};

