﻿#pragma once
/// @brief Базовый абстрактный класс
/// @details Базовый абстрактный класс для моделей ОУ

class AbstractModel {
	public:
		AbstractModel(){};

		/// @brief Метод, реализующий моделирование ОУ
		/// @details Метод реализует моделирование ОУ в соответсвии с задаными параметрами
		virtual void run() = 0;

		/// @brief Метод,задающий параметры для моделирования
		virtual void setParameters(int, int, double, double) = 0;

		virtual ~AbstractModel() {}

	protected:
		/**
		*@var "T" - Длина отрезка времени
		*@var "tau" - Интервал квантования
		*@var "Yt" - Выходная температура
		*@var "Ut" - Тепло
		*/
		int T, tau;
		double Yt, Ut;
};