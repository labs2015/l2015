﻿///@file main.cpp
///@author Константин Навросюк
///@date 14.05.2015
#include <iostream>
#include "NonLinearModel.h"
#include "LinearModel.h"

using namespace std;
///@brief Функция, демонстрирующая работу моделей ОУ
///@details Запрашивает у пользователя данные, создаёт объекты по ним и запускает моделирование.
int main() {
	int T, tau, Yt, Ut;
	cout << "Enter T: "; cin >> T;
	cout << "Enter t: "; cin >> tau;
	cout << "Enter Yt: "; cin >> Yt;
	cout << "Enter Ut: "; cin >> Ut;
	NonLinearModel* nlModel = new NonLinearModel(T, tau, Ut, Yt);
	LinearModel* lModel = new LinearModel(T, tau, Ut, Yt);

	nlModel->run();
	lModel->run();
	system("PAUSE");
	return 0;
}