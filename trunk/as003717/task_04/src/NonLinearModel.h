﻿#pragma once
#include "AbstractModel.h"
/// @brief Класс, реализующий нелинейную модель ОУ
class NonLinearModel : public AbstractModel{
public:
	NonLinearModel(int, int, double, double);
	~NonLinearModel();

	/// @brief Метод, реализующий моделирование ОУ
	/// @details Метод реализует моделирование ОУ в соответсвии с задаными параметрами
	void run();

	/// @brief Метод,задающий параметры для моделирования
	void setParameters(int, int, double, double);
};