﻿/// @file NonLinearModel.cpp
#include "NonLinearModel.h"
#include <iostream>
using namespace std;
/**
*@param "T" - Длина отрезка времени
*@param "tau" - Интервал квантования
*@param "Yt" - Выходная температура
*@param "Ut" - Тепло
*/
NonLinearModel::NonLinearModel(int T, int tau, double Ut, double Yt){
	this->T = T;
	this->tau = tau;
	this->Ut = Ut;
	this->Yt = Yt;
}


void NonLinearModel::run(){
	int iter = T / tau;
	double YtNext = 0;
	double YtPrevious = Yt;

	cout << "Nonlinear model: " << endl;
	cout << "Y(0) = " << Yt << ";" << endl;
	for (int i = 0; i < iter; i++)
	{
		YtNext = 0.9*Yt - 0.001*YtPrevious*YtPrevious + Ut + sin(Ut);
		cout << "Y(" << i + 1 << ") = " << YtNext << ";" << endl;
		YtPrevious = Yt;
		Yt = YtNext;
	}
}
/**
*@param "T" - Длина отрезка времени
*@param "tau" - Интервал квантования
*@param "Yt" - Выходная температура
*@param "Ut" - Тепло
*/
void NonLinearModel::setParameters(int T, int tau, double Ut, double Yt){
	this->T = T;
	this->tau = tau;
	this->Ut = Ut;
	this->Yt = Yt;
}

NonLinearModel::~NonLinearModel(){}
