﻿#pragma once
#include "AbstractModel.h"
/// @brief Класс, реализующий линейную модель ОУ


class LinearModel : public AbstractModel {
public:
	LinearModel(int, int, double, double);
	~LinearModel();
	/// @brief Метод, реализующий моделирование ОУ
	/// @details Метод реализует моделирование ОУ в соответсвии с задаными параметрами
	void run();

	/// @brief Метод,задающий параметры для моделирования
	void setParameters(int, int, double, double);
};