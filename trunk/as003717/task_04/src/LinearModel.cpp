﻿/// @file LinearModel.cpp
#include "LinearModel.h"
#include <iostream>
using namespace std;
/**
*@param "T" - Длина отрезка времени
*@param "tau" - Интервал квантования
*@param "Yt" - Выходная температура
*@param "Ut" - Тепло
*/
LinearModel::LinearModel(int T, int tau, double Ut, double Yt){
	this->T = T;
	this->tau = tau;
	this->Ut = Ut;
	this->Yt = Yt;
}


void LinearModel::run(){
	int iter = T / tau;
	double YtNext = 0;
	cout << "Linear model: " << endl;
	cout << "Y(0) = " << Yt << ";" << endl;
	for (int i = 0; i < iter; i++)
	{
		YtNext = 0.988*Yt + 0.232*Ut;
		cout << "Y(" << i + 1 << ") = " << YtNext << ";" << endl;
		Yt = YtNext;
	}
}

/**
*@param "T" - Длина отрезка времени
*@param "tau" - Интервал квантования
*@param "Yt" - Выходная температура
*@param "Ut" - Тепло
*/
void LinearModel::setParameters(int T, int tau, double Ut, double Yt){
	this->T = T;
	this->tau = tau;
	this->Ut = Ut;
	this->Yt = Yt;
}

LinearModel::~LinearModel(){}