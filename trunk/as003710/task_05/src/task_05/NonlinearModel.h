#pragma once
#include "GeneralModel.h"
class NonlinearModel :
	public GeneralModel
{
private:
	double ytPrev;
public:
	NonlinearModel();
	///@brief ������ ������� �������� ������������� � ��������� ������� �������� �����������
	///@param y0 - ��������� ������� �������� �����������
	NonlinearModel(double y0);
	///@brief ������� �� ������� ������� �������� ����������� �� ������ ��������(���������� ������)
	///@param yt - ��������� ������� �������� �����������
	///@param ut - ������� �������� �������������
	double Model(double yt, double ut);
	~NonlinearModel();
};



