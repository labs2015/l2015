///@file main.cpp
/// \author ����� �.�
#include <iostream>
#include <string>
#include "LModel.h"
#include "NLModel.h"
using namespace std;

void linearModel()
{
	LModel lModel;
	lModel.print(10, 1, 5, 3);
}

void nonlinearModel()
{
	NLModel nModel;
	nModel.print(10, 1, 5, 3);
}
/// \brief main-����� ����� � ���������.
int main()
{

		string answer;
		cout << "List of models:" << endl;
		cout << "1. linear model" << endl;
		cout << "2. Nonlinear model" << endl;
		cout << "Choose number of model:" << endl;
		cin >> answer;
		if (answer == "1")
			linearModel();
		if (answer == "2")
			nonlinearModel();
		if (answer != "1" && answer != "2")
			cout << "error" << endl;
	system("pause");
	return 0;
}

