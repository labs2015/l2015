///@file LModel.
#include "LModel.h"
/// \image html Linear.png
void LModel::print(int _time, int _T, double _y, double _u)
{
	int count = _time / _T;
	for (int i = 0; i < count; i++)
	{
		_y = 0.988*_y + 0.232*_u;
		std::cout << _y << std::endl;
	}
}
