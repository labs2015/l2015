#pragma once




class GeneralModel
{
public:
	GeneralModel();
	virtual double Model(double yt, double ut) = 0;
	~GeneralModel();
};


