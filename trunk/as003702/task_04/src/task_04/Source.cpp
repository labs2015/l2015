/**
@file Source.cpp
@author ������ ����� �. ��-37
@date 14.05.2015
@image html LinearGraphic.png
*/
#include<iostream>
#include"GeneralModel.h"
#include"LinearModel.h"
#include"NonlinearModel.h"
#include<iostream>

using namespace std;
/**
@brief ������� �� ������� �������� �����������
*/
void main()
{
	LinearModel linMod(2.4, 50);
	NonlinearModel nonlinMod(2.4, 100);

	linMod.PrintModel(12);
	cout << endl;
	nonlinMod.PrintModel(10);
	system("PAUSE");
}