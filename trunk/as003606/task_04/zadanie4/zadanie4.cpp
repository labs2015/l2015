// zadanie4.cpp: ���������� ����� ����� ��� ����������� ����������.
//
/// \file zadanie4.cpp
/// \author ���� ������
#include "stdafx.h"
#include <iostream>
#include "baseModel.h"
#include "modelLinear.h"
#include "modelNonLinear.h"

using namespace std;
/// \brief _tmain �������� ������ ����� � ���������
int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "");
	modelLinear line;
	cout << "�������� ������ ���������." << endl;
	line.function(10, 20);
	modelNonLinear nonline;
	cout << "��������� ������ ���������." << endl;
	nonline.function(10, 20);
	return 0;
}

