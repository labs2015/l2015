/**
@file Main.cpp
@author ������ �.�. ��-36
@date 27.05.2015
*/
#include<iostream>
#include"Mod.h"
#include"Lin.h"
#include"Nonlin.h"
#include"Regulator.h"
#include<iostream>

using namespace std;
/**
@brief �������� ������������� ���-����������.
������� �� ������� �������� �����������

*/
void main()
{
	const double w = 40.0;

	Lin *linMod = new Lin();
	Nonlin *nonlinMod = new Nonlin(0);
	Regulator *regul = new Regulator(0.015, 0.4, 0.27, 0.75);

	double y = linMod->Model(0, 0);
	double u = regul->GetU(y, w);

	for (int i = 0; i < 200; i++)
	{
		y = linMod->Model(y, u);
		u = regul->GetU(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}
	cout << "NonLinear Model: " << endl;

	y = nonlinMod->Model(0, 0);
	u = regul->GetU(y, w);

	for (int i = 0; i < 200; i++)
	{
		y = nonlinMod->Model(y, u);
		u = regul->GetU(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}

	system("PAUSE");
}
