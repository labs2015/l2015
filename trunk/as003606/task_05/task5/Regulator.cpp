#include "Regulator.h"


Regulator::Regulator()
{
}
Regulator::Regulator(double Td, double T, double T0, double K)
{
	this->Td = Td;
	this->T = T;
	this->T0 = T0;
	this->K = K;

	q0 = K * (1 + (Td / T0));
	q1 = -K *(1 + 2 * (Td / T0) - (T0 / T));
	q2 = K * (Td / T0);
	u = 0;
	eFirst = 0;
	eSec = 0;
	eThird = 0;
}

double Regulator::GetU(double y, double w)
{
	eThird = eSec;
	eSec = eFirst;
	eFirst = w - y;
	u += (q0 * eFirst + q1 * eSec + q2 * eThird);
	return u;
}
Regulator::~Regulator()
{
}