#pragma once
#include "Mod.h"
class Nonlin :
	public Mod
{
private:
	double ytPrev;
public:
	Nonlin();
	///@brief ������ ������� �������� ������������� � ��������� ������� �������� �����������
	///@param y0 - ��������� ������� �������� �����������
	Nonlin(double y0);
	///@brief ������� �� ������� ������� �������� ����������� �� ������ ��������(���������� ������)
	///@param yt - ��������� ������� �������� �����������
	///@param ut - ������� �������� �������������
	double Model(double yt, double ut);
	~Nonlin();
};

