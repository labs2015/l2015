#pragma once
/// �����, ������������ �������� �����������
class Regulator
{
private:
	double q0, q1, q2;
	double T0, Td, T;
	double K;
	double eFirst, eSec, eThird;
	double u;

public:
	Regulator();
	Regulator(double Td, double T0, double T, double K);
	/// ����������� ut
	///@param y -  ����������� ������������
	///@param w - �������� ���������������� �������
	///@return - ut
	double GetU(double y, double w);
	~Regulator();
};
