#include "Nonlin.h"
#include<iostream>
#include<math.h>

Nonlin::Nonlin()
{
}

Nonlin::Nonlin(double y0)
{
	this->ytPrev = y0;
}

double Nonlin::Model(double yt, double ut)
{
	double resYt = 0;
	resYt = 0.9*yt - 0.001*ytPrev*ytPrev + ut + sin(ut);
	ytPrev = yt;
	return resYt;
};
Nonlin::~Nonlin()
{
}
