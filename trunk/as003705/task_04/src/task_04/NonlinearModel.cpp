#include "NonlinearModel.h"
#include<iostream>
#include<math.h>

NonlinearModel::NonlinearModel()
{
}

NonlinearModel::NonlinearModel(double y0, double ud)
{
	this->yd = y0;
	this->ud = ud;
}

void NonlinearModel::PrintModel(int d)
{
	int i = 0;
	double temp = yd;
	double yprev = 0;
	yd = 0.9*yd - 0.001*yprev*yprev + ud + sin(ud);
	yprev = temp;
	while (i < d)
	{
		std::cout << yd << std::endl;
		temp = yd;
		yd = 0.9*yd - 0.001*yprev*yprev + ud + sin(ud);
		yprev = temp;
		i++;
	}
};
NonlinearModel::~NonlinearModel()
{
}
