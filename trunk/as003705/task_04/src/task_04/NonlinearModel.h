#pragma once
///@image html NonlinearGraphic.png
#include "GeneralModel.h"
///@brief �������� �����, ����������� ���������� ������
/**
������� �������� ���������� ��� ��� ������������ ��������, � ���������� ������������ ����� � ������.

������ ���������� ���������� ����������� ����� �� ������ �������.
*/
class NonlinearModel :
	public GeneralModel
{
private:
	double yd;
	double ud;
public:
	NonlinearModel();
	///@brief ������� �������� �������� ������������� � ���������� �������� �������� �����������
	///@param y0 - ��������� ������� �������� �����������
	///@param ud - ������� �������� �������������
	NonlinearModel(double y0, double ud);
	///@brief ����� �� ������� �������� �������� ����������� �� ������ ��������(���������� ������)
	///@param d - ���������� ��������
	void PrintModel(int d);
	~NonlinearModel();
};



