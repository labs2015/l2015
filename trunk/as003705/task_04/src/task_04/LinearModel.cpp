#include "LinearModel.h"
#include<iostream>

LinearModel::LinearModel()
{
}

LinearModel::LinearModel(double yd, double ud)
{
	this->ud = ud;
	this->yd = yd;
}

void LinearModel::PrintModel(int d)
{
	int i = 0;
	while (i < d)
	{
		yd = 0.988*yd + 0.232*ud;
		std::cout << yd << std::endl;
		i++;
	}
};

LinearModel::~LinearModel()
{
}
