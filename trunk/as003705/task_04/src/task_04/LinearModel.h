#pragma once
///@image html LinearGraphic.png
/**
������� �������� �������� �.�. �����������  �������� ������� ���������� ����������� ����� � ������.

������� ���������� �������� ����������� ����� �� ������ �������.
*/
#include "GeneralModel.h"
///@brief �������� �����, ������� ��������� �������� ������
class LinearModel :
	public GeneralModel
{
private:
	double yd;
	double ud;
public:
	LinearModel();
	///@brief �������� ��������� ������� �������� ����������� � ������� �������� �������������
	///@param yd - ��������� �������� ����������� ����������� �� ����
	///@param ud - �������� ������������� ����������� �� ����
	LinearModel(double yd, double ud);
	///@brief ����� �� ������� �������� �� ���� �������� ����������� �� ������ ��������(�������� ������.)
	///@param d - ���������� ��������
	void PrintModel(int d);
	~LinearModel();
};
