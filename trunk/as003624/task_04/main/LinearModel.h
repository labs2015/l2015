#pragma once
#include "Model.h"
/// \brief ��������� ������ Model.
/// \details ����� ���� �����,������� ��������� �������� ������ ��������� �������.
class LinearModel :
	public Model
{
public:
	LinearModel();
	/// \brief ���������� ������ Temper �� ������������ ������ Model.
    /// \details �����,������� ���������� ��������� �������� ����������� �� ������� �������� ������.
     void Temper(int T,int t,double y_t,double u_t);
	~LinearModel();
};

