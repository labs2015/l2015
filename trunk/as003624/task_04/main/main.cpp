/**
@mainpage ������
@image html Graph.png
Linear: ������ ����������� ����� ���������� ���,��� � ��������� ������ ������� ����������� �������� � ���������� ������������ ����� ������ � �������.
NonLinear: ������ ����������� ����� ���������� ���,��� � ��������� ������ ������� ����������� ��� �������� � ���������� ������������ ����� ������ � �������.
*/
/// \file main.cpp
/// \brief ���������, ������������ ������ ����������, ��������� �������� � ���������� ������.
/// \author ����� �.�.
/// \date 10.05.2015

#include <iostream>
#include "LinearModel.h"
#include "NonlinearModel.h"
using namespace std;
/// \brief main - ����� ����� � ���������
int main()
{
	setlocale(LC_ALL, "rus");
	LinearModel a;
	NonlinearModel b;
	int T, t;
	double y_t, u_t;
	cout << "������� ����� ������� �������: ";
	cin >> T;
	cout << "������� �������� �����������: " ;
	cin >> t;
	cout << "������� �������� �����������: " ;
	cin >> y_t;
	cout << "������� �����: " ;
	cin >> u_t;
	cout << "�������� ������: "<<endl;
	a.Temper(T, t, y_t, u_t);
	cout << "���������� ������: " << endl;
	b.Temper(T, t, y_t, u_t);
	system("pause");
	return 0;
}