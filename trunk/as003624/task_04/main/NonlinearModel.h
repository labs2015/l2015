#pragma once
#include "Model.h"
/// \brief ��������� ������ Model.
/// \details ����� ���� �����, ������� ��������� ���������� ������ ��������� �������.
class NonlinearModel :
	public Model
{
public:
	NonlinearModel();
	/// \brief ���������� ������ Temper �� ������������ ������ Model.
     /// \details �����,������� ���������� ��������� �������� ����������� �� ������� ���������� ������.
     void Temper(int T, int t, double y_t, double u_t);
	~NonlinearModel();
};

