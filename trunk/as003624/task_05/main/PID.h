#pragma once
///�����,����������� PID-���������
class PID
{
private:
	double K;
	double u;
	double q0, q1, q2;
	double T, T0, Td;
	double e, e1, e2;
public:
	/*!
	\param K - ����������� ��������
	\param T - ���������� ��������������
	\param Td - ���������� �����������������
	*/
	PID(double T, double T0, double Td, double K);
	/*!
	��� �������� ������������ �����������
	\param _w - �������� ���������������� �������
	\param _y - �������� ����������
	\return u(t) - ����������� �����������
	*/
	double computation(double _w, double _y);
	~PID();
};

