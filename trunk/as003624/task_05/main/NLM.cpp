#include "NLM.h"

NLM::NLM(double y0, double u0)
{
	y1 = y0;
	u1 = u0;
}

double NLM::Modulation(double _y, double _u)
{
	double y = 0.9*_y - 0.001*y1*y1 + _u + sin(u1);
	y1 = _y;
	u1 = _u;
	return y;
}
