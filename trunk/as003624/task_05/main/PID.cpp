#include "PID.h"


PID::PID(double T_, double T0_, double Td_, double K_) :T(T_), T0(T0_), Td(Td_), K(K_)
{
	u = 0;
	e = e1 = e2 = 0;

	q0 = K * (1 + (Td / T0));
	q1 = -K *(1 + 2 * (Td / T0) - (T0 / T));
	q2 = K * (Td / T0);
}

double PID::computation(double w, double y)
{
	e2 = e1;
	e1 = e;
	e = w - y;
	return u += (q0 * e + q1 * e1 + q2 * e2);
}


PID::~PID()
{
}
