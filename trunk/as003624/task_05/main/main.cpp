/*!
\date 25.05.2015
\author ����� �.�.
*/
/*!
\mainpage ������ � �������������� PID-����������
\image html LM.png
\image html NLM.png
�� ������ ��������, ��� ���������� ������ �������� ������� ��� ��������.
*/

#include <iostream>
#include "LM.h"
#include "NLM.h"
#include "PID.h"
using namespace std;
void lM();
void nlM();
int main()
{
	cout << "Linear model:" << endl;
	lM();
	cout << "Nonlinear model:" << endl;
	nlM();
	system("pause");
	return 0;
}
void lM()
{
	double w;
	LM lm;
	PID contr(0.5, 0.21, 0.01, 0.5);
	cout << "Input w: ";
	cin >> w;
	double y = lm.Modulation(0, 0);
	double u = contr.computation(w, y);
    for (int i = 0; i < 170; i++)
	{
	    cout << "y = " << y << "\t" << "u = " << u << endl;
		y = lm.Modulation(y, u);
		u = contr.computation(w, y);
	}
	}

void nlM()
{
	double w;
	PID contr(0.5, 0.21, 0.01, 0.5);
	cout << "Input w: ";
	cin >> w;
	NLM nlm(0, 0);
	double y = nlm.Modulation(0, 0);
	double u = contr.computation(w, y);
	for (int i = 0; i < 170; i++)
	{
		cout << "y = " << y << "\t" << "u = " << u << endl;
		y = nlm.Modulation(y, u);
		u = contr.computation(w, y);
	}
	}