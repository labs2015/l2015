#pragma once
#include "GeneralModel.h"
///@brief  ����� ����������� �������� ������
class LinearModel :
	public GeneralModel
{
public:
	LinearModel();
///@brief ������� �� ������� ������� �������� ����������� �� ������ ��������(�������� ������)
///@param yt - ��������� ������� �������� �����������
///@param ut - ������� �������� �������������
///@return yt 
	double Model(double yt, double ut);
	~LinearModel();
};
