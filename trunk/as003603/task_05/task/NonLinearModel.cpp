#include "NonlinearModel.h"
#include<iostream>
#include<math.h>
#include"stdafx.h"

NonlinearModel::NonlinearModel()
{
}

NonlinearModel::NonlinearModel(double y0)
{
	this->ytPrev = y0;
}

double NonlinearModel::Model(double yt, double ut)
{
	double resYt = 0;
	resYt = 0.9*yt - 0.001*ytPrev*ytPrev + ut + sin(ut);
	ytPrev = yt;
	return resYt;
};
NonlinearModel::~NonlinearModel()
{
}
