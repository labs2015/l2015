///@mainpage
///@image html LinearGraphic.png
///������� �������� �������� �.�. ��� ��������� ������� ���������� ����������� ����� � ������.
///@image html NonlinearGraphic.png
///������� �������� ���������� �.�. ������������ ��������, ������� ���������� ����������� ����� � ������. 
/**
@file Source.cpp
@author ������� �.�. ��-36
@date 13.05.2015
@image html LinearGraphic.png
*/
#include<iostream>
#include"GeneralModel.h"
#include"LinearModel.h"
#include"NonlinearModel.h"
#include<iostream>

using namespace std;
/**
@brief ������� �� ������� �������� �����������
*/
void main()
{
	LinearModel linMod(5, 50);
	NonlinearModel nonlinMod(5, 100);

	linMod.PrintModel(12);
	cout << endl;
	nonlinMod.PrintModel(10);
	cout << endl;
	system("PAUSE");
}