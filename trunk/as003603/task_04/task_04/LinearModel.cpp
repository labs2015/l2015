#include "LinearModel.h"
#include<iostream>

LinearModel::LinearModel()
{
}

LinearModel::LinearModel(double yt, double ut)
{
	this->ut = ut;
	this->yt = yt;
}

void LinearModel::PrintModel(int t)
{
	int i = 0;
	while (i < t)
	{
		yt = 0.988*yt + 0.232*ut;
		std::cout << yt << std::endl;
		i++;
	}
};

LinearModel::~LinearModel()
{
}
/**
@image html NonlinearGraphic.png
*/