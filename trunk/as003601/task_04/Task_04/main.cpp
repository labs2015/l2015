///@file
///@author �������� �.�.
///@date 13.05.2015
#include <iostream>
#include <cmath>
using namespace std;

class Model
{
public:
	double Y0 = 0;
	///@brief ������� getY0 ���������� ��������� �������� �������� �����������
	double getY0()
	{
		return Y0;
	}

	///@brief ������� SetY0 ������������� ��������� �������� �������� �����������
	void setY0()
	{
		this->Y0;
	}

	///@brief ������� show ������� ���������� � �������� �����������
	virtual void show(int(*u)(int)) = 0;
};

///@brief ������� u ���������� �������� �����, ������������ � ������� � ������ ������� t
int u(int t)
{
	return 120;
}

class Model1 : public Model
{
public:

	void show(int(*ut)(int))
	{
		cout << "Dlia lineinoi modeli: " << endl;
		cout << " Y " << endl;
		cout << Y0 << endl;
		double y = Y0;
		for (int i = 1; i < 10; i++)
		{
			y = 0.988*y + 0.232*ut(i - 1);
			cout << y << endl;
		}

	}
};

class Model2 : public Model
{

public:
    double Y1 = 23;
	///@brief ������� getY1 ���������� �������� �������� ����������� ��� ���������� ������
	double getY1()
	{
		return Y1;
	}

	///@brief ������� setY1 ������������� �������� �������� ����������� ��� ���������� ������
	void setY1()
	{
		this->Y1;
	}

	void show(int(*ut)(int))
	{
		double y[10];
		cout << "Dlia nelineinoi modeli: " << endl;
		cout << " Y " << endl;
		cout << Y0 << endl << Y1 << endl;
		y[0] = Y0;
		y[1] = Y1;
		for (int i = 2; i < 10; i++)
		{
			y[i] = 0.9*y[i - 1] - 0.001*pow(y[i - 2], 2) + ut(i - 1) + sin(ut(i - 2));
			cout << y[i] << endl;
		}

	}
};

int main()
{
	Model1 mod1;
	Model2 mod2;
	mod1.show(&u);
	cout << endl << endl;
	mod2.show(&u);
	system("pause");
}