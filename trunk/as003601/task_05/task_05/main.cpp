///@file
///@author �������� �.�.
///@27.05.2015
#include <iostream>

using namespace std;

class Model
{
public:
	///@brief ���������� ��������� � �������� ����������� �������
	virtual double getTemperature(double y, double u) = 0;
};

class Model1 : public Model
{
public:
	double getTemperature(double y, double u)
	{
		return 0.988*y + 0.232*u;
	}
};


class Model2 : public Model
{
protected:
	double yL = 20;
public:
	///@brief ���������� �������� �������� ����������� �������
	double getY1()
	{
		return yL;
	}

	///@brief ������������� �������� �������� ����������� �������
	void setY1(double yL)
	{
		this->yL = yL;
	}

	double getTemperature(double y, double u)
	{
		double result = 0.9*y - 0.001*yL*yL + u + sin(u);
		yL = y;
		return result;
	}
};

class PID
{
private:
	double T0, Td, T, k, q0, q1, q2;
	double e1 = 0, e2 = 0, e3 = 0, u = 0;
public:

	///@brief ������� SetT0 ������������� �������� ��� T0
	void SetT0(double t)
	{
		T0 = t;
	}
	double getT0()
	{
		return T0;
	}

	///@brief ������� SetTd ������������� �������� ��� Td
	void SetTd(double t)
	{
		Td = t;
	}
	double getTd()
	{
		return Td;
	}
	///@brief ������� SetT ������������� �������� ��� T
	void SetT(double t)
	{
		T = t;
	}
	double getT()
	{
		return T;
	}
	///@brief ������� SetK ������������� �������� ��� �
	void SetK(double K)
	{
		k = K;
	}
	double getK()
	{
		return k;
	}
	///@brief ������� CalcQ ������������ �������� q
	void CalcQ()
	{
		q0 = k * (1 + (Td / T0));
		q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = k * (Td / T0);
	}

	double Calc(double y, double w)
	{
		e3 = e2;
		e2 = e1;
		e1 = w - y;
		u += (q0 * e1 + q1 * e2 + q2 * e3);
		return u;
	}

};

///@brief ������ ������������� ����������� �������
int main()
{
	Model1 m1;
	Model2 m2;
	PID pid;
	pid.SetK(0.4);
	pid.SetT(0.4);
	pid.SetT0(0.1);
	pid.SetTd(0.02);
	pid.CalcQ();
	m2.setY1(0);

	double w = 50, y = m1.getTemperature(0, 0), u = pid.Calc(y, w);


	for (int i = 0; i < 50; i++)
	{
		y = m1.getTemperature(y, u);
		u = pid.Calc(y, w);
		cout << y << "    " << u << endl;
	}

	cout << endl << endl << endl;


	y = m2.getTemperature(0, 0);
	u = pid.Calc(y, w);


	for (int i = 0; i < 50; i++)
	{
		y = m2.getTemperature(y, u);
		u = pid.Calc(y, w);
		cout.width(5);
		cout << y << "    " << u << endl;
	}

	system("Pause");
}