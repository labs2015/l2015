///@file main.cpp
/// \author ����� �.�.
/// \mainpage My documentation
/// \image html NeLin.png
/// \image html Lin.png
///������ �������� ������� ����������� ������, �.�. ������ ����� ����� �������� �� ���������� �� �������� "���������� ����������� ����������� � ������������� 0,988 , � ����� ����� ��������� � ������������� 0.232", �.�. ������������ ���.
//������ ���������� ������� ���������� �� ������, �.�.������ ����� ����� ���������� �� ������ � �������������� ���������� ��������
#include <iostream>
#include <string>
#include "Lin.h"
#include "NeLin.h"
using namespace std;

void lin()
{
	Lin lModel;
	lModel.Modeling(10, 1, 5, 3);
}

void nelin()
{
	NeLin nModel;
	nModel.Modeling(10, 1, 5, 3);
}
/// \brief main-����� ����� � ���������.
int main()
{
	setlocale(LC_ALL, "Russian");
	while (true)
	{
		string answer;
		cout << "������ �������:" << endl;
		cout << "1. Lin ������" << endl;
		cout << "2. NeLin ������" << endl;
		cout << "�������� ����� ������:" << endl;
		cin >> answer;
		if (answer == "1")
			lin();
		if (answer == "2")
			nelin();
		if (answer != "1" && answer != "2")
			cout << "error" << endl;

		cout << "������ ���������? (y/n)" << endl;
		
		cin >> answer;
		if (answer != "y")
			break;
	}
	
	system("pause");
	return 0;
}

